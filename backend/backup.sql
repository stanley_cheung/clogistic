-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 08, 2015 at 09:07 AM
-- Server version: 5.5.42-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clogistic`
--

-- --------------------------------------------------------

--
-- Table structure for table `db1_table`
--

CREATE TABLE IF NOT EXISTS `db1_table` (
  `_id` int(8) NOT NULL AUTO_INCREMENT,
  `col_0` varchar(30) DEFAULT NULL,
  `col_1` varchar(30) DEFAULT NULL,
  `col_2` varchar(30) DEFAULT NULL,
  `col_3` varchar(30) DEFAULT NULL,
  `col_4` varchar(30) DEFAULT NULL,
  `col_5` int(8) DEFAULT NULL,
  `col_6` int(8) DEFAULT NULL,
  `col_7` int(8) DEFAULT NULL,
  `col_8` double DEFAULT NULL,
  `col_9` double DEFAULT NULL,
  `col_10` double DEFAULT NULL,
  `col_11` varchar(30) DEFAULT NULL,
  `col_12` varchar(30) DEFAULT NULL,
  `col_13` varchar(30) DEFAULT NULL,
  `col_14` varchar(30) DEFAULT NULL,
  `col_15` int(8) DEFAULT NULL,
  `col_16` int(8) DEFAULT NULL,
  `col_17` int(8) DEFAULT NULL,
  `col_18` double DEFAULT NULL,
  `col_19` double DEFAULT NULL,
  `col_20` double DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=811 ;

--
-- Dumping data for table `db1_table`
--

INSERT INTO `db1_table` (`_id`, `col_0`, `col_1`, `col_2`, `col_3`, `col_4`, `col_5`, `col_6`, `col_7`, `col_8`, `col_9`, `col_10`, `col_11`, `col_12`, `col_13`, `col_14`, `col_15`, `col_16`, `col_17`, `col_18`, `col_19`, `col_20`) VALUES
(802, 'DO001', 'COM001', 'First Entry', '', '', 20150910, 140000, 0, 2, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(803, 'DO002', 'COM001', 'Second Entry', '', '', 20150910, 143000, 0, 2, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(804, 'DO003', 'COM002', 'Third Entry', '', '', 20150911, 150000, 0, 1, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(805, 'DO004', 'COM003', 'Try Mcdonal', '', '', 20150920, 153000, 0, 1, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(809, 'DO005', 'COM003', 'NA', '', '', 20150928, 143000, 0, 1, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(810, 'DO006', 'COM004', '6th', '', '', 20150919, 183000, 0, 1, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `db2_table`
--

CREATE TABLE IF NOT EXISTS `db2_table` (
  `_id` int(8) NOT NULL AUTO_INCREMENT,
  `col_0` varchar(30) DEFAULT NULL,
  `col_1` varchar(30) DEFAULT NULL,
  `col_2` varchar(30) DEFAULT NULL,
  `col_3` varchar(30) DEFAULT NULL,
  `col_4` varchar(30) DEFAULT NULL,
  `col_5` int(8) DEFAULT NULL,
  `col_6` int(8) DEFAULT NULL,
  `col_7` int(8) DEFAULT NULL,
  `col_8` double DEFAULT NULL,
  `col_9` double DEFAULT NULL,
  `col_10` double DEFAULT NULL,
  `col_11` varchar(30) DEFAULT NULL,
  `col_12` varchar(30) DEFAULT NULL,
  `col_13` varchar(30) DEFAULT NULL,
  `col_14` varchar(30) DEFAULT NULL,
  `col_15` int(8) DEFAULT NULL,
  `col_16` int(8) DEFAULT NULL,
  `col_17` int(8) DEFAULT NULL,
  `col_18` double DEFAULT NULL,
  `col_19` double DEFAULT NULL,
  `col_20` double DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=807 ;

--
-- Dumping data for table `db2_table`
--

INSERT INTO `db2_table` (`_id`, `col_0`, `col_1`, `col_2`, `col_3`, `col_4`, `col_5`, `col_6`, `col_7`, `col_8`, `col_9`, `col_10`, `col_11`, `col_12`, `col_13`, `col_14`, `col_15`, `col_16`, `col_17`, `col_18`, `col_19`, `col_20`) VALUES
(801, 'COM001', '大文有限公司', 'Kowloon', 'Happy Road 201', '', 29384723, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(805, 'COM003', 'Kesia Co Ltd', 'Hong Kong', 'Way 201', 'ABC Build', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(804, 'COM002', '發利公司', 'Hong Kong', 'Way 201', 'ABC Build', 93591191, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(806, 'COM004', 'HTC Co Ltd', 'NT', '31 New World Road', 'Sun Mansion', 25364758, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `db3_table`
--

CREATE TABLE IF NOT EXISTS `db3_table` (
  `_id` int(8) NOT NULL AUTO_INCREMENT,
  `col_0` varchar(30) DEFAULT NULL,
  `col_1` varchar(30) DEFAULT NULL,
  `col_2` varchar(30) DEFAULT NULL,
  `col_3` varchar(30) DEFAULT NULL,
  `col_4` varchar(30) DEFAULT NULL,
  `col_5` int(8) DEFAULT NULL,
  `col_6` int(8) DEFAULT NULL,
  `col_7` int(8) DEFAULT NULL,
  `col_8` double DEFAULT NULL,
  `col_9` double DEFAULT NULL,
  `col_10` double DEFAULT NULL,
  `col_11` varchar(30) DEFAULT NULL,
  `col_12` varchar(30) DEFAULT NULL,
  `col_13` varchar(30) DEFAULT NULL,
  `col_14` varchar(30) DEFAULT NULL,
  `col_15` int(8) DEFAULT NULL,
  `col_16` int(8) DEFAULT NULL,
  `col_17` int(8) DEFAULT NULL,
  `col_18` double DEFAULT NULL,
  `col_19` double DEFAULT NULL,
  `col_20` double DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=809 ;

--
-- Dumping data for table `db3_table`
--

INSERT INTO `db3_table` (`_id`, `col_0`, `col_1`, `col_2`, `col_3`, `col_4`, `col_5`, `col_6`, `col_7`, `col_8`, `col_9`, `col_10`, `col_11`, `col_12`, `col_13`, `col_14`, `col_15`, `col_16`, `col_17`, `col_18`, `col_19`, `col_20`) VALUES
(801, 'DO001', 'SCC102', '', '', '', 0, 0, 0, 5, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(802, 'DO001', 'SZE100', '', '', '', 0, 0, 0, 3, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(803, 'DO001', 'CCM200', '', '', '', 0, 0, 0, 24, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(804, 'DO001', 'CCK220', '', '', '', 0, 0, 0, 70, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(805, 'DO001', 'CMC011', '', '', '', 0, 0, 0, 8, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(806, 'DO002', 'stat2001', '', '', '', 0, 0, 0, 3, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(807, 'DO003', 'SEEM2001', '', '', '', 0, 0, 0, 50, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0),
(808, 'DO004', 'SCC102', '', '', '', 0, 0, 0, 123, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
