<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<title>CFT cLogistic Management System</title>

<meta name="viewport" content="width=device-width, initial-scale=1">



<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">

<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/underscore-min.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/f.js"></script>

<link rel="stylesheet" href="css/style.css" type="text/css" />

</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.php">cLogistic</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="index.php">所有訂單</a></li>
        <li><a href="do.php">訂單詳細</a></li>  
        <li><a href="co.php">公司列表</a></li>  
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="https://sg2plcpnl0018.prod.sin2.secureserver.net:2083/cpsess3025331546/3rdparty/phpMyAdmin/index.php#PMAURL-0:index.php?db=&table=&server=1&target=&token=a07ee520765393fa00a5b413f8422b50">資料庫</a></li>
      </ul>
    </div>
  </div>
</nav>