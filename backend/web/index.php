<?php 
include('header.php');?>


<?php

include ("connection.php");


if ($_GET["way"] == "status") {
	$del_l_id = $_GET["idd"];
	$sql_del = "UPDATE `db1_table` SET `col_8`='1' WHERE `_id`=$del_l_id";

	if ($conn->query($sql_del) === TRUE) {
	    echo '<div class="container"><div class="alert alert-info" role="alert">訂單狀態更新</div></div>';
	} else {
	    echo "Error: " . $sql_del . "<br>" . $conn->error;
	}
}

if ($_GET["way"] == "del_l") {
	$del_l_id = $_GET["idd"];
	$sql_del = "DELETE FROM `db1_table` WHERE `_id` = '$del_l_id' ";

	if ($conn->query($sql_del) === TRUE) {
	    echo '<div class="container"><div class="alert alert-info" role="alert">訂單已被刪除</div></div>';
	} else {
	    echo "Error: " . $sql_del . "<br>" . $conn->error;
	}
}


if (isset($_POST["confirm"])) {

	$cocode = $_POST["cocode"];
	$dnno = $_POST["dnno"];
	$remark = $_POST["remark"];
	$date = $_POST["date"];
	$time = $_POST["time"];
	$car = $_POST["car"];


	$sql_add = "INSERT INTO `db1_table` (`_id`, `col_0`, `col_1`, `col_2`, `col_3`, `col_4`, `col_5`, `col_6`, `col_7`, `col_8`, `col_9`, `col_10`, `col_11`, `col_12`, `col_13`, `col_14`, `col_15`, `col_16`, `col_17`, `col_18`, `col_19`, `col_20`) VALUES ('','$dnno','$cocode','$remark','','','$date','$time','0','1','0', '0', '','','','','$car','0','0','0','0','0')";

	if ($conn->query($sql_add) === TRUE) {
	    echo '<div class="container"><div class="alert alert-info" role="alert">訂單已新增</div></div>';
	} else {
	    echo "Error: " . $sql_add . "<br>" . $conn->error;
	}

}

$sql = "SELECT * FROM db1_table ORDER BY `_id` DESC";
$result = $conn->query($sql);


?>

<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>加入訂單 : </h3>
				<form method="POST">
				   訂單編號 : <input type="text" name="dnno">
				   公司名稱 : <input type="text" name="cocode">
				   記錄 : <input type="text" name="remark">
				   送貨日期 : <input type="text" name="date">
				   送貨時間 : <input type="text" name="time">
				   
				   貨車 : <input type="text" name="car">

				  <input type="submit" class="btn btn-default mar10" role="group" name="confirm" value="確認">
				</form>
			</div>
		</div>

		
		<div class="table-responsive"> 
			<table class="table-color studenttb">

			<tr class="head-b">
				<th>#ID</th>
				<th>訂單編號</th>
				<th>公司名稱</th>
				<th>記錄</th>
				<th>相片</th>
				<th>送貨日期</th>
				<th>時間</th>
				
				<th>到達日期</th>
				<th>到達時間</th>

				<th>貨車</th>

				<th>情況</th>
				<th>刪除</th>
				<th>更改狀態</th>
			</tr>

			<?php while($row = $result->fetch_row()){ ?>
			
			<tr id="<?php echo $row[0]?>" class="s-list">
				<td><?php echo $row[0]?></td>
				<td><?php echo $row[1]?></td>
				<td><?php echo $row[2]?></td>
				<td><?php echo $row[3]?></td>
				<td><?php echo $row[4]?></td>
				<td><?php echo $row[6]?></td>
				<td><?php echo $row[7]?></td>
				
				<td><?php echo $row[10]?></td>
				<td><?php echo $row[8]?></td>
				
				<td><?php echo $row[16]?></td>

				<?php switch ($row[9]) {
					case '0':
						$status = "Not Online";
						break;
					case '1':
						$status = "On The Way";
						break;
					case '2':
						$status = "Delivered";
						break;
					case '3':
						$status = "Special Case";
						break;
					case '4':
						$status = "Closed";
						break;
					case '5':
						$status = "Deleted";
						break;
				}?>
				<td><?php echo $status?></td>
				<td><a href=<?php echo $_SERVER['PHP_SELF'].'?way=del_l&idd='.$row[0] ?> onclick="return confirm('刪除 <?php echo $name ?> DO ?')"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></a></td>
				<td><a href=<?php echo $_SERVER['PHP_SELF'].'?way=status&idd='.$row[0] ?> onclick="return confirm('重設 <?php echo $name ?> 狀態 ?')"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></a></td>
			</tr>
			
			<?php } ?>
		
			</table>
		</div>
</div>