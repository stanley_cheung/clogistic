package com.example.user.logistic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;

import java.io.File;


public class PageTab extends AppCompatActivity implements AsyncResponse{


    // Declaring Your View and Variables

    Toolbar toolbar;

    ViewPager pager;
    ViewPagerAdpater adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"On the way","Delivered","Tab3"};
    int Numboftabs =2;

    Bundle bundle;

    private String sort_by;

    //Database function
    String return_pass;
    String table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_tab);

        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);



        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdpater(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_page_main, menu);
            setTitle(Html.fromHtml("<font color='#003f2e'>Logistic</font>"));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.by_time) {
            Log.w("by_time","1");
            sort_by = "by time";
            reload();
            return true;
        }

        if (id == R.id.by_dis) {
            Log.w("by_dis", "1");
            sort_by = "by district";
            reload();
            return true;
        }

        if (id == R.id.action_refresh) {
            Log.w("refresh","1");
            dl_all();
            return true;
        }

        if (id == R.id.logout) {
            Log.w("logout","1");
            confirm_dia();
            return true;
        }

        if (id == R.id.cancel){
            Intent intent = new Intent(PageTab.this,PageCancel.class);
            startActivity(intent);
        }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        confirm_dia();
    }

    private void dl_all() {


            SysSqlAdapter2 sql = new SysSqlAdapter2(PageTab.this, 2, null, null);
            sql.delegate = this;
            sql.execute("clogistic", table);
            sql.setListener(new CallBackListener() {
                @Override
                public void callback() {
                    setContentView(R.layout.page_tab);


                    // Creating The Toolbar and setting it as the Toolbar for the activity

                    toolbar = (Toolbar) findViewById(R.id.tool_bar);
                    setSupportActionBar(toolbar);


                    // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
                    adapter = new ViewPagerAdpater(getSupportFragmentManager(), Titles, Numboftabs);

                    // Assigning ViewPager View and setting the adapter
                    pager = (ViewPager) findViewById(R.id.pager);
                    pager.setAdapter(adapter);

                    // Assiging the Sliding Tab Layout View
                    tabs = (SlidingTabLayout) findViewById(R.id.tabs);
                    tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

                    // Setting Custom Color for the Scroll bar indicator of the Tab View
                    tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
                        @Override
                        public int getIndicatorColor(int position) {
                            return getResources().getColor(R.color.tabsScrollColor);
                        }
                    });

                    // Setting the ViewPager For the SlidingTabsLayout
                    tabs.setViewPager(pager);
                }
            });

    }

    private void confirm_dia() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PageTab.this);

        builder.setMessage("Are you sure?").setTitle("Logout")
        ;

        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent in = new Intent(PageTab.this, PageMain.class);
                startActivity(in);
                finish();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //Database download function
    public void processFinish(String output){
        return_pass = output;
    }

    public void reload(){
        Log.w("reload","success");
        setContentView(R.layout.page_tab);


        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdpater(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }

    public String getMyData(){
        Log.w("passing",sort_by);
        return sort_by;
    }

}
