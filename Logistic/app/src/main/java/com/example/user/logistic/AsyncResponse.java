package com.example.user.logistic;


public interface AsyncResponse {
    void processFinish(String output);
}