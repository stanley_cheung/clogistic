package com.example.user.logistic;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SysSync {

    /**General Declars**/

    String epassword;


    /** Call Back Method **/

    CallBackListener listener;

    public void setListener(CallBackListener cbl){
        listener = cbl;
    }

    public AsyncResponse delegate = null;

    /** Collect Database **/

    private static AsyncHttpClient client = new AsyncHttpClient();

    private static final String BASE_URL = "http://cftandroid.com/clogistic/";

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);

    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    /** Personal Method **/

    public void login(String username, String password){

        RequestParams params = new RequestParams();
        params.put("way", "login");
        params.put("username", username);
        epassword = encrypt(password);
        Log.w("SysSync UN : ",username);
        Log.w("SysSync PS : ",password);
        Log.w("SysSync EP : ",epassword);

        post("dbaccess.php", params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {

                delegate.processFinish("SysSync RS(Error) : " + responseString);
                listener.callback();
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {

                if (epassword.equals(responseString)) {
                    delegate.processFinish("SysSync RS(Success)");
                    //delegate.processFinish("SysSync RS(Success) : " + responseString);
                } else {
                    delegate.processFinish("SysSync RS(Fail) : " + responseString);
                }
                listener.callback();


            }

        });

    }

    public static String encrypt(String string) {
        byte[] hash;

        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);

        for (byte b : hash) {
            int i = (b & 0xFF);
            if (i < 0x10) hex.append('0');
            hex.append(Integer.toHexString(i));
        }

        return hex.toString();
    }


}
