package com.example.user.logistic;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;


public class PageCancel extends AppCompatActivity {

    Toolbar toolbar;

    ImageView list_icon;

    TextView date;

    //For expandable ist view
    ExpandableListView expListView;
    ArrayList<HashMap<String,String>> listDataHeader = new ArrayList<HashMap<String,String>>();;
    ArrayList<HashMap<String,String>> listDataChild = new ArrayList<HashMap<String,String>>();;
    private android.widget.ExpandableListAdapter listAdapter;

    //For database
    SysDbAdapter dba;
    Cursor put;
    String item_details;

    //Personal Tab

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_cancel);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        dba = new SysDbAdapter(this);

        date = (TextView)findViewById(R.id.textView41);

        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        String month = c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
        int year = c.get(Calendar.YEAR);

        String now = day + ", " + month + ", " + year;
        date.setText(now);

        expListView = (ExpandableListView) findViewById(R.id.del_listview);

        list_icon = (ImageView) findViewById(R.id.imageView4);

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            // Keep track of previous expanded parent
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                // Collapse previous parent if expanded.
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    expListView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });

        //prepareListData();


    }

    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");

        prepareListData();

        super.onResume();
    }

    public void prepareListData() {

        listAdapter = null;
        listDataHeader.clear();
        listDataChild.clear();

        dba.open();

        put = dba.GetSpecificItem("db1_table", "col_8","4");
        put.moveToFirst();

        for(int row = 0; row < put.getCount(); row++){
            HashMap<String, String> item = new HashMap<String, String>();
            item.clear();

            Cursor put2 = dba.GetSpecificItem("db2_table", "col_0", put.getString(2));
            put2.moveToFirst();
            Cursor put3 = dba.GetSpecificItem("db3_table", "col_0", put.getString(1));
            put3.moveToFirst();

            //Business Name
            item.put( "col1", put2.getString(2));

            //Address
            item.put( "col2", put2.getString(3) + ", " + put2.getString(4) + ", " + put2.getString(5));

            //Phone
            item.put("col3", put2.getString(6));

            //Date Time Restructuring
            String date = put.getString(6).substring(0,4) + "-" + put.getString(6).substring(4,6) + "-" + put.getString(6).substring(6,8);
            String time = put.getString(7).substring(0,2) + ":" + put.getString(7).substring(2,4);
            item.put( "col4", time);

            //DO no.
            item.put( "col5", put.getString(1));

            //Total Item
            item.put( "col6", "No. of stocks : " + Integer.toString(put3.getCount()));

            //Get all item to list
            item_details = "";
            for(int i = 0; i<put3.getCount(); i++){
                item_details = item_details + put3.getString(2) + " x " + put3.getString(9) + " / " + "\n";
                put3.moveToNext();
            }
            item.put("col7", item_details);

            //Status
            item.put( "col8", put.getString(9));


            listDataHeader.add(item);

            put.moveToNext();
        }


        dba.close();


        listAdapter = new com.example.user.logistic.ExpandableListAdapter2(this, listDataHeader, listDataChild);

        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                TextView do_no = (TextView) v.findViewById(R.id.lblListItem);

                Intent intent = new Intent(PageCancel.this, PageDetails.class);
                intent.putExtra("dono", do_no.getText().toString());
                startActivity(intent);

                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_page_cancel, menu);
        setTitle(Html.fromHtml("<font color='#003f2e'>Cancelled cases</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        return true;
    }




}
