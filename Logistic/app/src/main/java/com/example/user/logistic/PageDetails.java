package com.example.user.logistic;

import android.app.AlertDialog;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 4/9/2015.
 */
public class PageDetails extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    String grp_id;

    TextView dn;
    TextView company;
    TextView adrs;
    TextView tel;
    TextView time;
    TextView date;
    TextView person;
    TextView count;
    TextView details;

    SysDbAdapter dba;
    Cursor put;
    Cursor put1;
    Cursor put2;
    String item_details;


    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        setContentView(R.layout.page_details);

        dba = new SysDbAdapter(PageDetails.this);

        grp_id = getIntent().getStringExtra("dono");
        Log.w("12345",grp_id);

        get_details();

        setUpMapIfNeeded();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void get_details() {

        dn = (TextView)findViewById(R.id.textView23);
        dn.setText(grp_id);

        company = (TextView)findViewById(R.id.textView16);
        adrs = (TextView)findViewById(R.id.textView17);
        tel = (TextView)findViewById(R.id.textView18);
        time = (TextView)findViewById(R.id.textView20);
        date = (TextView)findViewById(R.id.textView21);
        person = (TextView)findViewById(R.id.textView25);
        count = (TextView)findViewById(R.id.textView27);
        details = (TextView)findViewById(R.id.textView28);

        dba.open();

        put = dba.GetSpecificItem("db1_table","col_0",grp_id);
        put.moveToFirst();

        put1 = dba.GetSpecificItem("db2_table","col_0",put.getString(2));
        put1.moveToFirst();

        put2 = dba.GetSpecificItem("db3_table","col_0",grp_id);
        put2.moveToFirst();

        company.setText(put1.getString(2));
        adrs.setText(put1.getString(3) + ", " + put1.getString(4) + ", " + put1.getString(5));
        tel.setText(put1.getString(6));
        time.setText(put.getString(7).substring(0,2) + ":" + put.getString(7).substring(2,4));
        date.setText(put.getString(6).substring(0,4) + "-" + put.getString(6).substring(4,6) + "-" + put.getString(6).substring(6,8));

        count.setText(Integer.toString(put2.getCount()));

        item_details = "";
        for(int i = 0; i<put2.getCount(); i++){
            item_details = item_details + put2.getString(2) + " x " + put2.getString(9) + " / " + "\n";
            put2.moveToNext();
        }

        details.setText(item_details);

        dba.close();

    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {

        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> adrs_geo = geoCoder.getFromLocationName(adrs.getText().toString(), 5);
            if (adrs_geo.size() > 0) {

                Double lat = adrs_geo.get(0).getLatitude();
                Double lon = adrs_geo.get(0).getLongitude();

                Log.d("find_map",Double.toString(lat)+", "+Double.toString(lon));

                final LatLng place = new LatLng(lat, lon);
                moveMap(place);

            }
            else {
                Log.d("find_map","fail");
                AlertDialog.Builder adb = new AlertDialog.Builder(PageDetails.this);
                adb.setTitle("Google Map");
                adb.setMessage("Please Provide the Proper Place");
                adb.setPositiveButton("Close",null);
                adb.show();
                LatLng place = new LatLng(22.2783758, 114.1651826);
                moveMap(place);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void moveMap(LatLng place) {

        CameraPosition cameraPosition =
                new CameraPosition.Builder()
                        .target(place)
                        .zoom(15)
                        .build();


        //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        addMarker(place,  company.getText().toString() , tel.getText().toString());
    }

    private void addMarker(LatLng place, String title, String snippet) {
        /*BitmapDescriptor icon =
                BitmapDescriptorFactory.fromResource(R.drawable.mini_cft_logo);*/

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(place)
                .title(title)
                .snippet(snippet);

        mMap.addMarker(markerOptions);
    }

}
