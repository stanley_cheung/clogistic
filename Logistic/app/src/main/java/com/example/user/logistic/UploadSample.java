package com.example.user.logistic;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class UploadSample extends AppCompatActivity {
    String filePath;
    String imgFolderPath;//this is the folder containing images.
    String fileName;//you have to manually put an image file with this name into the imgFolderPath for your development.

    String url = "http://cftandroid.com/fileUpload/php/upload.php";//you have to place the upload.php file onto web server first. Once this app upload file, the web server then catch the image file and will save the file into the folder mentioned in the upload.php file.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_main);

        fileName = "test.jpg";
        imgFolderPath= Environment.getExternalStorageDirectory()+"/cLogistic/"; //you should create this folder first. Or use program to check if this existed to prevent crash.
        uploadImage(imgFolderPath+fileName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_page_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void uploadImage(String fileFullPath){

        filePath=fileFullPath;
        //fileName="/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20151202-WA0014.jpg";
        //fileName="/sdcard/LSM_Screen.jpg";

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            File myFile = new File(filePath);
            RequestParams params = new RequestParams();
            params.setForceMultipartEntityContentType(true);
            params.put("file", myFile);

            client.post(getApplicationContext(), url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                    String s = null;
                    try {
                        s = new String(responseBody, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    theToast(s);
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    theToast("Failed");
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void theToast(String s){
        Toast.makeText(getApplicationContext(), "訊息: " + s, Toast.LENGTH_SHORT).show();
    }

}
