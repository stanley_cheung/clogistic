package com.example.user.logistic;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class PageMain extends AppCompatActivity implements AsyncResponse{


    SysSync ss = new SysSync();
    String return_pass;

    TextView tv_un;
    TextView tv_ps;
    String un;
    String ps;

    public static String DLUsr; // New Global (Static) String to Save Download User

    public void processFinish(String output){
        return_pass = output;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.page_main);

        tv_un = (TextView) findViewById(R.id.editText);
        tv_ps = (TextView) findViewById(R.id.editText2);
        ss.delegate = this;

        /*File file = new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail1.jpg");
        File file2 = new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail2.jpg");
        File file3 = new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail3.jpg");
        File file4= new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail4.jpg");
        file.delete();
        file2.delete();
        file3.delete();
        file4.delete();*/

        Button btn_1 = (Button)findViewById(R.id.button);
        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                un = tv_un.getText().toString();
                ps = tv_ps.getText().toString();

                ss.login(un,ps);
                ss.setListener(new CallBackListener() {
                    @Override
                    public void callback() {
                        if(return_pass.equals("SysSync RS(Success)")){
                            Log.e("PageMain D : ",return_pass.toString());
                            Toast.makeText(PageMain.this,"Online Login",Toast.LENGTH_SHORT).show();


                            DLUsr = un; // Save Login User to Download

                            Intent intent = new Intent(PageMain.this, PageTab.class);
                            startActivity(intent);
                        }else{
                            Log.e("PageMain D : ",return_pass.toString());
                            Toast.makeText(PageMain.this,"Local Login",Toast.LENGTH_SHORT).show();

                            DLUsr = un; // Save Login User to Download

                            Intent intent = new Intent(PageMain.this, PageTab.class);
                            startActivity(intent);
                        }
                    }
                });

                /*
                Intent intent = new Intent(PageMain.this, PageTab.class);
                startActivity(intent);
                */
            }
        });


        btn_1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent intent = new Intent(PageMain.this, PageSql.class);
                startActivity(intent);
                return true;
            }
        });


        final View activityRootView = findViewById(R.id.layout_page_main);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > 100) {
                    final ScrollView scrollview = ((ScrollView) findViewById(R.id.scrollView));
                    scrollview.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollview.scrollTo(0, 1000);
                        }
                    });
                }
            }
        });


        /*
        Button test_btn = (Button)findViewById(R.id.test);

        test_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animation = new Intent(PageMain.this,AccordianSampleActivity.class);
                startActivity(animation);
            }
        });
        */

        Button map_btn = (Button)findViewById(R.id.map);
        map_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(PageMain.this, MapsActivity.class);
                startActivity(map);
            }
        });

        Button call_btn = (Button)findViewById(R.id.call);
        call_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "tel:1878200";
                Intent call = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                startActivity(call);
            }
        });








        /*File file = new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail1.jpg");
        File file2 = new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail2.jpg");
        File file3 = new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail3.jpg");
        File file4= new File(Environment.getExternalStorageDirectory(), File.separator+"Logistic_images"+File.separator+"thumbnail4.jpg");
        file.delete();
        file2.delete();
        file3.delete();
        file4.delete();*/

    }

}
