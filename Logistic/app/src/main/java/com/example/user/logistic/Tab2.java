package com.example.user.logistic;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;


public class Tab2 extends Fragment {

    TextView date;


    //For expandable ist view
    ExpandableListView expListView;
    ArrayList<HashMap<String,String>> listDataHeader = new ArrayList<HashMap<String,String>>();;
    ArrayList<HashMap<String,String>> listDataChild = new ArrayList<HashMap<String,String>>();;
    private android.widget.ExpandableListAdapter listAdapter;

    //For database
    SysDbAdapter dba;
    Cursor put;
    String item_details;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.tab_1,container,false);

        dba = new SysDbAdapter(getActivity());

        date = (TextView)v.findViewById(R.id.textView40);

        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        String month = c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
        int year = c.get(Calendar.YEAR);

        String now = day + ", " + month + ", " + year;
        date.setText(now);

        expListView = (ExpandableListView) v.findViewById(R.id.o_listview);

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            // Keep track of previous expanded parent
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                // Collapse previous parent if expanded.
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    expListView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });

        expListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        expListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                mode.setTitle(expListView.getCheckedItemCount()+" selected cases");
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {


                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.contextual_menu, menu);
                //menu.findItem(R.id.item_delete);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch(item.getItemId()){
                    case R.id.item_delete:
                        mode.finish();
                        return true;
                    default:
                        return false;
                }

            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                listAdapter.isEmpty();
            }
        });

        //prepareListData();

        return v;
    }

    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");

        prepareListData();

        super.onResume();
    }

    public void prepareListData() {

        listAdapter = null;
        listDataHeader.clear();
        listDataChild.clear();

        dba.open();

        //Status = Delivered
        put = dba.GetSpecificItem("db1_table","col_8","8");
        put.moveToFirst();



        for(int row = 0; row < put.getCount(); row++){
            HashMap<String, String> item = new HashMap<String, String>();
            item.clear();

            Cursor put2 = dba.GetSpecificItem("db2_table", "col_0", put.getString(2));
            put2.moveToFirst();
            Cursor put3 = dba.GetSpecificItem("db3_table", "col_0", put.getString(1));
            put3.moveToFirst();

            //Business Name
            item.put( "col1", put2.getString(2));

            //Address
            item.put( "col2", put2.getString(3) + ", " + put2.getString(4) + ", " + put2.getString(5));

            //Phone
            item.put("col3", put2.getString(6));

            //Date Time Restructuring
            String date = put.getString(6).substring(0,4) + "-" + put.getString(6).substring(4,6) + "-" + put.getString(6).substring(6,8);
            String time = put.getString(8).substring(0,2) + ":" + put.getString(8).substring(2,4);
            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DATE);
            String month = c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
            int year = c.get(Calendar.YEAR);
            String now = day + "-" + month + "-" + year;
            item.put( "col4", now + "   " + time);

            //DO no.
            item.put( "col5", put.getString(1));

            //Total Item
            item.put( "col6",  Integer.toString(put3.getCount()) + " Stocks in total");

            //Get all item to list
            item_details = "";
            for(int i = 0; i<put3.getCount(); i++){
                item_details = item_details + put3.getString(2) + " x " + put3.getString(9) + " / " + "\n";
                put3.moveToNext();
            }
            item.put( "col7", " ( " + item_details + " )" );

            item.put( "col8", put.getString(9));

            listDataHeader.add(item);
            put.moveToNext();
        }

//        put = dba.GetSpecificItem("db1_table","col_8","8");  // Another Status
//        put.moveToFirst();
//
//
//        //Status = Damage
//        for(int row = 0; row < put.getCount(); row++){
//            HashMap<String, String> item = new HashMap<String, String>();
//
//            Cursor put2 = dba.GetSpecificItem("db2_table", "col_0", put.getString(2));
//            put2.moveToFirst();
//            Cursor put3 = dba.GetSpecificItem("db3_table", "col_0", put.getString(1));
//            put3.moveToFirst();
//
//            //Business Name
//            item.put( "col1", put2.getString(2));
//
//            //Address
//            item.put( "col2", put2.getString(3) + ", " + put2.getString(4) + ", " + put2.getString(5));
//
//            //Phone
//            item.put("col3", put2.getString(6));
//
//            //Date Time Restructuring
//            String date = put.getString(6).substring(0,4) + "-" + put.getString(6).substring(4,6) + "-" + put.getString(6).substring(6,8);
//            String time = put.getString(8).substring(0,2) + ":" + put.getString(8).substring(2,4);
//
//            Calendar c = Calendar.getInstance();
//            int day = c.get(Calendar.DATE);
//            String month = c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
//            int year = c.get(Calendar.YEAR);
//            String now = day + "-" + month + "-" + year;
//            item.put( "col4", now + "  " + time);
//
//            //DO no.
//            item.put( "col5", put.getString(1));
//
//            //Total Item
//            item.put( "col6", Integer.toString(put3.getCount()) + "Stocks in total");
//
//            //Get all item to list
//            item_details = "";
//            for(int i = 0; i<put3.getCount(); i++){
//                item_details = item_details + put3.getString(2) + " x " + put3.getString(9) + " / " + "\n";
//                put3.moveToNext();
//            }
//            item.put( "col7", " ( " + item_details + " ) ");
//
//            item.put( "col8", put.getString(9));
//
//            listDataHeader.add(item);
//            put.moveToNext();
//        }


        dba.close();

        listAdapter = new com.example.user.logistic.ExpandableListAdapter2(getActivity(), listDataHeader, listDataChild);


        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                TextView do_no = (TextView) v.findViewById(R.id.lblListItem);

                Intent intent = new Intent(getActivity(), PageDetails.class);
                intent.putExtra("dono", do_no.getText().toString());
                getActivity().startActivity(intent);

                return false;
            }
        });



    }



}

