package com.example.user.logistic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class PageSql extends Activity implements AsyncResponse{

    Spinner sp_db_1;
    Spinner sp_tb_1;

    Button bt_download_1;
    Button bt_upload_1;
    Button bt_image_1;

    ListView lv_setting;

    String database = "clogistic";
    String table = "db1_table";
    String return_pass;

    Cursor put;

    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
    private SimpleAdapter adapter;

    SysDbAdapter dba = new SysDbAdapter(this);
    F f = new F();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.page_sql);

        lv_setting = (ListView) findViewById(R.id.setting_list);

        build_db();
        set_button();
        set_spinner();
        del_item();

    }

    private void set_spinner() {
        sp_db_1 = (Spinner)findViewById(R.id.sp_db);
        sp_tb_1 = (Spinner)findViewById(R.id.sp_tb);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.sp_db, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_db_1.setAdapter(adapter1);
        sp_db_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        database = "clogistic";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.sp_tb, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_tb_1.setAdapter(adapter2);
        sp_tb_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        table = "db1_table";
                        put_all();
                        break;
                    case 1:
                        table = "db2_table";
                        put_all();
                        break;
                    case 2:
                        table = "db3_table";
                        put_all();
                        break;
                    case 3:
                        table = "db4_table";
                        put_all();
                        break;
                    case 4:
                        table = "db5_table";
                        put_all();
                        break;
                    case 5:
                        table = "db6_table";
                        put_all();
                        break;
                    case 6:
                        table = "db7_table";
                        put_all();
                        break;
                    case 7:
                        table = "db8_table";
                        put_all();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void dl_all() {

        dba.open();
        dba.Delete_All(table);

        try{

            SysSqlAdapter sql =new SysSqlAdapter(PageSql.this, 2, null, null);
            sql.delegate = this;
            sql.execute(database,table);
            sql.setListener(new CallBackListener() {
                @Override
                public void callback() {
                    try{

                        JSONArray json = new JSONArray(return_pass);

                        for(int i=0;i<json.length();i++){

                            JSONObject e = json.getJSONObject(i);

                            new String(e.getString("col_1").getBytes("ISO-8859-1"),"UTF-8");

                            dba.InsertData(
                                    table,
                                    e.getString("col_0"),
                                    e.getString("col_1"),
                                    new String(e.getString("col_2").getBytes("ISO-8859-1"),"UTF-8"),
                                    e.getString("col_3"),
                                    e.getString("col_4"),
                                    e.getInt("col_5"),
                                    e.getInt("col_6"),
                                    e.getInt("col_7"),
                                    e.getDouble("col_8"),
                                    e.getDouble("col_9"),
                                    e.getDouble("col_10"),
                                    e.getString("col_11"),
                                    e.getString("col_12"),
                                    new String(e.getString("col_13").getBytes("ISO-8859-1"),"UTF-8"),
                                    new String(e.getString("col_14").getBytes("ISO-8859-1"),"UTF-8"),
                                    e.getInt("col_15"),
                                    e.getInt("col_16"),
                                    e.getInt("col_17"),
                                    e.getDouble("col_18"),
                                    e.getDouble("col_19"),
                                    e.getDouble("col_20")
                            );

                        }

                    }catch(Exception e){

                        f.t(PageSql.this,"Network Error - PageSql - dl_all");
                        Log.e("PageSql - dl_all", e.toString());

                    }
                    dba.close();

                    put_all();
                }
            });

        }

        catch(Exception e)
        {
            f.t(PageSql.this,"Network Error - PageSql - dl_all" + e.toString() + "F");
            Log.e("PageSql - dl_all", e.toString());
        }


    }

    private void dl_all_big() {

        dba.open();
        dba.Delete_All(table);

        try{

            SysSqlAdapter sql =new SysSqlAdapter(PageSql.this, 2, null, null);
            sql.delegate = this;
            sql.execute(database,table);
            sql.setListener(new CallBackListener() {
                @Override
                public void callback() {
                    try{

                        JSONArray json = new JSONArray(return_pass);

                        for(int i=0;i<json.length();i++){

                            JSONObject e = json.getJSONObject(i);

                            new String(e.getString("col_1").getBytes("ISO-8859-1"),"UTF-8");

                            dba.InsertData_big(
                                    table,
                                    e.getString("col_0"),
                                    e.getString("col_1"),
                                    e.getInt("col_2"),
                                    e.getInt("col_3"),
                                    e.getInt("col_4"),
                                    e.getString("col_5"),
                                    e.getString("col_6"),
                                    e.getString("col_7"),
                                    e.getInt("col_8"),
                                    e.getInt("col_9"),
                                    e.getInt("col_10"),
                                    e.getInt("col_11"),
                                    e.getInt("col_12"),
                                    e.getInt("col_13"),
                                    e.getString("col_14"),
                                    e.getInt("col_15"),
                                    e.getInt("col_16"),
                                    e.getInt("col_17"),
                                    e.getDouble("col_18"),
                                    e.getDouble("col_19"),
                                    e.getDouble("col_20"),
                                    e.getDouble("col_21"),
                                    e.getString("col_22"),
                                    e.getString("col_23"),
                                    e.getString("col_24"),
                                    e.getString("col_25"),
                                    e.getString("col_26"),
                                    e.getString("col_27"),
                                    e.getString("col_28"),
                                    e.getString("col_29")
                            );

                        }

                    }catch(Exception e){

                        f.t(PageSql.this,"Network Error - PageSql - dl_all");
                        Log.e("PageSql - dl_all", e.toString());

                    }
                    dba.close();

                    put_all();
                }
            });

        }

        catch(Exception e)
        {
            f.t(PageSql.this,"Network Error - PageSql - dl_all" + e.toString() + "F");
            Log.e("PageSql - dl_all", e.toString());
        }


    }

    private void put_all() {

        list.clear();

        dba.open();

        put = dba.GetAllRow(table);

        for(int row = 0; row < put.getCount(); row++){
            HashMap<String, String> item = new HashMap<String, String>();
            item.put( "_id", Integer.toString(put.getInt(0)));
            item.put( "col_0", put.getString(1));
            item.put( "col_1", put.getString(2));
            item.put( "col_2", put.getString(3));
            item.put( "col_3", put.getString(4));
            item.put( "col_4", put.getString(5));
            item.put( "col_5", Integer.toString(put.getInt(6)));
            item.put( "col_6", Integer.toString(put.getInt(7)));
            item.put( "col_7", Integer.toString(put.getInt(8)));
            item.put( "col_8", Double.toString(put.getDouble(9)));
            item.put( "col_9", Double.toString(put.getDouble(10)));
            item.put( "col_10", Double.toString(put.getDouble(11)));
            item.put( "col_11", put.getString(12));
            item.put( "col_12", put.getString(13));
            item.put( "col_13", put.getString(14));
            item.put( "col_14", put.getString(15));
            item.put( "col_15", Integer.toString(put.getInt(16)));
            item.put( "col_16", Integer.toString(put.getInt(17)));
            item.put( "col_17", Integer.toString(put.getInt(18)));
            item.put( "col_18", Double.toString(put.getDouble(19)));
            item.put( "col_19", Double.toString(put.getDouble(20)));
            item.put( "col_20", Double.toString(put.getDouble(21)));
            list.add(item);
            put.moveToNext();
        }

        dba.close();
        show_list();
    }

    private void build_db() {
        dba.open();
        //dba.droptable("db7_table");
        dba.Create_DB1(getApplicationContext());
        dba.Create_DB2(getApplicationContext());
        dba.Create_DB3(getApplicationContext());
        dba.Create_DB4(getApplicationContext());
        dba.Create_DB5(getApplicationContext());
        dba.Create_DB6(getApplicationContext());
        dba.Create_DB7(getApplicationContext());
        dba.Create_DB8(getApplicationContext());

        dba.close();
    }

    private void show_list() {
        lv_setting.setAdapter(null);

        adapter = new SimpleAdapter(PageSql.this,
                list,
                R.layout.list_main,
                new String[] {
                        "_id",
                        "col_0",
                        "col_1",
                        "col_2",
                        "col_3",
                        "col_4",
                        "col_5",
                        "col_6",
                        "col_7",
                        "col_8",
                        "col_9",
                        "col_10",
                        "col_11",
                        "col_12",
                        "col_13",
                        "col_14",
                        "col_15",
                        "col_16",
                        "col_17",
                        "col_18",
                        "col_19",
                        "col_20"},
                new int[] {
                        R.id._id,
                        R.id.col_0,
                        R.id.col_1,
                        R.id.col_2,
                        R.id.col_3,
                        R.id.col_4,
                        R.id.col_5,
                        R.id.col_6,
                        R.id.col_7,
                        R.id.col_8,
                        R.id.col_9,
                        R.id.col_10,
                        R.id.col_11,
                        R.id.col_12,
                        R.id.col_13,
                        R.id.col_14,
                        R.id.col_15,
                        R.id.col_16,
                        R.id.col_17,
                        R.id.col_18,
                        R.id.col_19,
                        R.id.col_20
                });

        lv_setting.setAdapter(adapter);
        lv_setting.setTextFilterEnabled(true);
    }

    private void set_button() {
        bt_download_1 = (Button)findViewById(R.id.bt_download);
        bt_upload_1 = (Button)findViewById(R.id.bt_upload);
        bt_image_1 = (Button)findViewById(R.id.bt_image);

        bt_download_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    dl_all();


            }
        });

        bt_upload_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String return_pass = new SysSqlAdapter(PageSql.this, 1, null, null).execute(database, "admin").get(20000, TimeUnit.MILLISECONDS);
                    f.t(PageSql.this, return_pass);
                } catch (Exception e) {
                    f.t(PageSql.this, e.toString());
                }
            }
        });

        bt_image_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_image();
            }
        });


    }

    private void del_item() {
        lv_setting.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final TextView this_id = (TextView) view.findViewById(R.id._id);

                AlertDialog.Builder builder = new AlertDialog.Builder(PageSql.this);
                builder.setMessage("Confirm delete this row?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dba.open();
                                dba.Delete_row(table, "_id", this_id.getText().toString());
                                dba.close();

                                put_all();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });

                builder.show();

                return true;
            }
        });
    }

    private void add_image() {
        dba.open();

        /*
        dba.UpdateItem_i("db2_table","col_5","100137","col_7",R.drawable.j100137);
        dba.UpdateItem_i("db2_table","col_5","100138","col_7",R.drawable.j100138);
        dba.UpdateItem_i("db2_table","col_5","102184","col_7",R.drawable.j102184);
        dba.UpdateItem_i("db2_table","col_5","102856","col_7",R.drawable.j102856);
        dba.UpdateItem_i("db2_table","col_5","104707","col_7",R.drawable.j104707);
        dba.UpdateItem_i("db2_table","col_5","104906","col_7",R.drawable.j104906);

        dba.UpdateItem_i("db2_table","col_5","103725","col_7",R.drawable.reynolds);
        dba.UpdateItem_i("db2_table","col_5","103979","col_7",R.drawable.reynolds);
        dba.UpdateItem_i("db2_table","col_5","103071","col_7",R.drawable.reynolds);
        dba.UpdateItem_i("db2_table","col_5","104077","col_7",R.drawable.tena);
        dba.UpdateItem_i("db2_table","col_5","104078","col_7",R.drawable.tena);
        dba.UpdateItem_i("db2_table","col_5","104195","col_7",R.drawable.tena);
        dba.UpdateItem_i("db2_table","col_5","104196","col_7",R.drawable.tena);
        dba.UpdateItem_i("db2_table","col_5","104911","col_7",R.drawable.vinda);

        dba.InsertData("db5_table","FILTERA","F&B","Food & Beverage","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","FILTERA","Toiletry Paper","Toiletry Paper Products","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","FILTERA","Incontinence","Incontinence Products","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","FILTERB","REYNOLDS","REYNOLDS","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","FILTERB","DIAMOND","DIAMOND","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","FILTERB","VINDA","VINDA","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","FILTERB","TENA","TENA","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);

        dba.InsertData("db5_table","NEWS","Marketing Review 021","","","",R.drawable.pdf5,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","NEWS","Team Memo 288","","","",R.drawable.pdf6,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","Client Discount","","","",R.drawable.pdf1,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","Product Promotion","","","",R.drawable.pdf2,0,0,0,0,0,"","","","",0,0,0,0,0,0);


        dba.Delete_row("db5_table","col_0","NEWS");
        dba.Delete_row("db5_table","col_0","DISCOUNT");
        */

        dba.InsertData("db5_table","DISCOUNT","20150716_Diamond_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_GoodMaid_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_GP_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_GP_01x.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Hearttex_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Hearttex_02.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Hearttex_03.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Lion_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_nepia_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_nepia_02.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Sealen_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Tenna_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Vinda_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Vinda_02.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Vinda_03.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_Vinda_04.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);
        dba.InsertData("db5_table","DISCOUNT","20150716_zOthers_01.pdf","","","",0,0,0,0,0,0,"","","","",0,0,0,0,0,0);

        dba.close();
    }

    public void processFinish(String output){
        return_pass = output;
    }
}


/*

dba.InsertData_big(
                        table,
                        e.getString("col_0"),
                        e.getString("col_1"),
                        e.getInt("col_2"),
                        e.getInt("col_3"),
                        e.getInt("col_4"),
                        e.getString("col_5"),
                        e.getString("col_6"),
                        e.getString("col_7"),
                        e.getInt("col_8"),
                        e.getInt("col_9"),
                        e.getInt("col_10"),
                        e.getInt("col_11"),
                        e.getInt("col_12"),
                        e.getInt("col_13"),
                        e.getString("col_14"),
                        e.getInt("col_15"),
                        e.getInt("col_16"),
                        e.getInt("col_17"),
                        e.getDouble("col_18"),
                        e.getDouble("col_19"),
                        e.getDouble("col_20"),
                        e.getDouble("col_21"),
                        e.getString("col_22"),
                        e.getString("col_23"),
                        e.getString("col_24"),
                        e.getString("col_25"),
                        e.getString("col_26"),
                        e.getString("col_27"),
                        e.getString("col_28"),
                        e.getString("col_29")
                );

 */