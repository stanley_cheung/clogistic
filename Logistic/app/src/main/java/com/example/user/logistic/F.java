package com.example.user.logistic;

import android.content.Context;
import android.widget.Toast;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by fung1020hk on 18/6/15.
 */
public class F {

    public String rp(String input){
        String op = input.replace(" ", "%20").replaceAll("\\r|\\n", "%20").trim();
        return op;
    }

    public void t(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public String date(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Date curDate = new Date(System.currentTimeMillis()) ;;
        String str = formatter.format(curDate);

        return str;
    }

    public String time(){
        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");

        Date curDate = new Date(System.currentTimeMillis()) ;;
        String str = formatter.format(curDate);

        return str;
    }

    public String date_fm(String date) {
                String r_date =
                date.substring(0,4) + "年" +
                date.substring(4,6) + "月" +
                date.substring(6,8) + "日" +
                date.substring(8,10) + ":" +
                date.substring(10,12) + ":" +
                date.substring(12,14);
        return r_date;
    }

    String ip = "cftandroid.com";
}
