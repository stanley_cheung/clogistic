package com.example.user.logistic;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


public class SysSqlAdapter2 extends AsyncTask<String,Integer,String> {
    private TextView statusField,roleField;
    private int way;
    private Context context;
    private int count;
    StringBuilder sb;

    public AsyncResponse delegate = null;
    CallBackListener mListener;

    SysDbAdapter dba;
    F f = new F();

    private ProgressDialog progressDialog;

    public SysSqlAdapter2(Context context, int way, TextView statusField, TextView roleField){
        this.context = context;
        this.way = way;
        this.statusField = statusField;
        this.roleField = roleField;
        this.dba = new SysDbAdapter(context);
    }

    protected void onPreExecute(){
        Log.d("SQL", "PreExecute");
        showProgressDialog();
    }

    protected String doInBackground(String... arg0) {

        dba.open();
        //dba.droptable("db7_table");
        dba.Create_DB1(context);
        dba.Create_DB2(context);
        dba.Create_DB3(context);
        dba.close();

        if(way == 2) {

            ArrayList<String> db = new ArrayList<String>();
            db.add("db1_table");
            db.add("db2_table");
            db.add("db3_table");

            for (int a = 0; a < 3; a++) {

                ////////////////////////////////////////////////////////
                /////// Download by online database ///////////////////
                //////////////////////////////////////////////////////
                try{

                    String DLUsr = PageMain.DLUsr;

                    String link = "http://"+f.ip+"/clogistic/dbaccess.php";

                    String data  = URLEncoder.encode("way", "UTF-8") + "=" + URLEncoder.encode("get", "UTF-8");
                    data += "&" + URLEncoder.encode("database", "UTF-8") + "=" + URLEncoder.encode("clogistic", "UTF-8");
                    data += "&" + URLEncoder.encode("table", "UTF-8") + "=" + URLEncoder.encode(db.get(a), "UTF-8");

                    if (DLUsr != "")
                    data += "&" + URLEncoder.encode("usr", "UTF-8") + "=" + URLEncoder.encode(DLUsr, "UTF-8");

                    Log.w("DLUsr", DLUsr);
                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

                    sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null)
                    {
                        sb.append(line);
                        break;
                    }

                }catch (Exception e) {
                    Log.e("SysSql1", e.toString());
                    return "13rnhx87fnybwdcunx97rfgiwyg";
                }

                ////////////////////////////////////////////////////////
                /////// Import to local database    ///////////////////
                //////////////////////////////////////////////////////

                try {
                    dba.open();
                    dba.Delete_All(db.get(a));
                    Log.w("db", "del : "+db.get(a));
                    dba.close();

                    JSONArray json = new JSONArray(sb.toString());

                    for (int i = 0; i < json.length(); i++) {

                        JSONObject e = json.getJSONObject(i);

                        new String(e.getString("col_1").getBytes("ISO-8859-1"), "UTF-8");

                        dba.open();
                        Log.w("db", "open : " + db.get(a));
                        dba.InsertData(
                                db.get(a),
                                e.getString("col_0"),
                                e.getString("col_1"),
                                new String(e.getString("col_2").getBytes("ISO-8859-1"), "UTF-8"),
                                e.getString("col_3"),
                                e.getString("col_4"),
                                e.getInt("col_5"),
                                e.getInt("col_6"),
                                e.getInt("col_7"),
                                e.getDouble("col_8"),
                                e.getDouble("col_9"),
                                e.getDouble("col_10"),
                                e.getString("col_11"),
                                e.getString("col_12"),
                                new String(e.getString("col_13").getBytes("ISO-8859-1"), "UTF-8"),
                                new String(e.getString("col_14").getBytes("ISO-8859-1"), "UTF-8"),
                                e.getInt("col_15"),
                                e.getInt("col_16"),
                                e.getInt("col_17"),
                                e.getDouble("col_18"),
                                e.getDouble("col_19"),
                                e.getDouble("col_20")
                        );
                        Log.w("db", "insert : " + db.get(a));
                        dba.close();



                        Log.w("db", "close : " + db.get(a));

                    }

                } catch (Exception e) {

                    Log.e("PageSql - dl_all", e.toString());

                }



            }


        }else{
            Toast.makeText(context, "Have not set any way", Toast.LENGTH_SHORT).show();
            return null;
        }

        return null;
    }

    protected void onProgressUpdate(Integer... progress) {
        Log.d("SQL", "ProgressUpdate");
    }

    protected void onPostExecute(String result) {
        Log.d("SQL", "PostExecute");
        delegate.processFinish(result);
        mListener.callback();
        progressDialog.dismiss();


    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(context);

        progressDialog.setMessage("Loading Please Wait"); // message

        progressDialog.show();
    }

    public void setListener(CallBackListener listener){
        mListener = listener;
    }


}
