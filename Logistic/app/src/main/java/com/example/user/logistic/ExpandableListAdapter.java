package com.example.user.logistic;


import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<HashMap<String,String>> _listDataHeader; // header titles
    // child data in format of header title, child title
    private ArrayList<HashMap<String,String>> _listDataChild;

    F f = new F();


    public ExpandableListAdapter(Context context, ArrayList<HashMap<String,String>> listDataHeader,
                                                  ArrayList<HashMap<String,String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;

        this._listDataChild = listChildData;

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText1 = (String) this._listDataHeader.get(groupPosition).get("col5");
        final String childText2 = (String) this._listDataHeader.get(groupPosition).get("col6");
        final String childText3 = (String) this._listDataHeader.get(groupPosition).get("col7");

        final String headerTel = this._listDataHeader.get(groupPosition).get("col3");
        Log.w("Btn", Integer.toString(groupPosition));


        //Cancel child icon
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.listview_details, null);

            ImageButton btn_call = (ImageButton)convertView.findViewById(R.id.call_btn);
            btn_call.setFocusable(false);

            btn_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w("Call button", "clicked and call: "+headerTel);

                    Context context = v.getContext();

                    String url = "tel:"+headerTel;
                    Intent call = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    context.startActivity(call);

                }
            });


        }



        TextView txtListChild1 = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txtListChild2 = (TextView) convertView.findViewById(R.id.text2);
        TextView txtListChild3 = (TextView) convertView.findViewById(R.id.text3);

        txtListChild1.setText(childText1);
        txtListChild2.setText(childText2);
        txtListChild3.setText(childText3);


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        //String headerTitle = (String) getGroup(groupPosition).toString();
        String headerCo = this._listDataHeader.get(groupPosition).get("col1");
        String headerAdd = this._listDataHeader.get(groupPosition).get("col2");
        String headerTel = this._listDataHeader.get(groupPosition).get("col3");
        String headerExp =  this._listDataHeader.get(groupPosition).get("col4");
        String headerDo = this._listDataHeader.get(groupPosition).get("col5");
        String headerDis = this._listDataHeader.get(groupPosition).get("col8");

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.listview_order, null);





            ImageButton grp_btn_1 = (ImageButton)convertView.findViewById(R.id.grp_btn1);
            grp_btn_1.setFocusable(false);

            grp_btn_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    View pv = (View) v.getParent();
                    TextView do_no = (TextView) pv.findViewById(R.id.dono);

                    Context context = v.getContext();
                    Intent intent = new Intent(context, PageConfirm.class);
                    intent.putExtra("dono", do_no.getText().toString());
                    context.startActivity(intent);

                }
            });

            /*
            ImageButton grp_btn_2 = (ImageButton)convertView.findViewById(R.id.grp_btn2);
            grp_btn_2.setFocusable(false);


            grp_btn_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, PageMain.class);
                    context.startActivity(intent);

                }
            });
            */

        }

        ImageView icon = (ImageView)convertView.findViewById(R.id.imageView4);
        if(headerDis.equals("NT")){
            icon.setImageResource(R.drawable.snt);
        }
        if(headerDis.equals("Kowloon")){
            icon.setImageResource(R.drawable.skln);
        }
        if(headerDis.equals("Hong Kong")){
            icon.setImageResource(R.drawable.shk);
        }

        TextView tv_company = (TextView) convertView
                .findViewById(R.id.company);
        //tv_company.setTypeface(null, Typeface.BOLD);
        tv_company.setText(headerCo);

        TextView tv_adrs = (TextView) convertView
                .findViewById(R.id.adrs);
        //tv_adrs.setTypeface(null, Typeface.BOLD);
        tv_adrs.setText(headerAdd);

        TextView tv_tel = (TextView) convertView
                .findViewById(R.id.tel);
        //tv_tel.setTypeface(null, Typeface.BOLD);
        tv_tel.setText(headerTel);

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        //lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerExp);

        TextView dono = (TextView) convertView
                .findViewById(R.id.dono);
        dono.setText(headerDo);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void notifyDataSetChanged() {
        this.notifyDataSetChanged();
    }

}

