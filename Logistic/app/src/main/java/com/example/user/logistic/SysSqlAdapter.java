package com.example.user.logistic;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;


public class SysSqlAdapter extends AsyncTask<String,Integer,String> {
    private TextView statusField,roleField;
    private int way;
    private Context context;
    private int count;

    public AsyncResponse delegate = null;
    CallBackListener mListener;

    F f = new F();

    private ProgressDialog progressDialog;

    public SysSqlAdapter(Context context, int way, TextView statusField, TextView roleField){
        this.context = context;
        this.way = way;
        this.statusField = statusField;
        this.roleField = roleField;
    }

    protected void onPreExecute(){
        Log.d("SQL", "PreExecute");
        showProgressDialog();
    }

    protected String doInBackground(String... arg0) {

        //Login
        if(way == 1) {

            try {

                String database = (String) arg0[0];
                String username = (String) arg0[1];

                String link = "http://"+f.ip+"/clogistic/dbaccess.php";

                String data  = URLEncoder.encode("way", "UTF-8") + "=" + URLEncoder.encode("login", "UTF-8");
                data += "&" + URLEncoder.encode("database", "UTF-8") + "=" + URLEncoder.encode(database, "UTF-8");
                data += "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                    break;
                }
                return sb.toString();

            } catch (Exception e) {
                Log.e("SysSql1", e.getMessage());
                return "13rnhx87fnybwdcunx97rfgiwyg";
            }

        }

        if(way == 2) {

            try {

                String database = (String) arg0[0];
                String table = (String) arg0[1];

                String link = "http://"+f.ip+"/clogistic/dbaccess.php";

                String data  = URLEncoder.encode("way", "UTF-8") + "=" + URLEncoder.encode("get", "UTF-8");
                data += "&" + URLEncoder.encode("database", "UTF-8") + "=" + URLEncoder.encode(database, "UTF-8");
                data += "&" + URLEncoder.encode("table", "UTF-8") + "=" + URLEncoder.encode(table, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                    break;
                }

                Log.w("SQL1", sb.toString());

                return sb.toString();

            } catch (Exception e) {
                Log.e("SysSql1", e.getMessage());
                return "13rnhx87fnybwdcunx97rfgiwyg";
            }

        }if(way == 3) {

            try {

                String table = (String) arg0[0];
                String col = (String) arg0[1];
                String like = (String) arg0[2];
                String where = (String) arg0[3];
                String update = (String) arg0[4];

                String link = "http://"+f.ip+"/clogistic/dbaccess.php";

                String data  = URLEncoder.encode("way", "UTF-8") + "=" + URLEncoder.encode("update_s", "UTF-8");
                data += "&" + URLEncoder.encode("database", "UTF-8") + "=" + URLEncoder.encode("clogistic", "UTF-8");
                data += "&" + URLEncoder.encode("table", "UTF-8") + "=" + URLEncoder.encode(table, "UTF-8");
                data += "&" + URLEncoder.encode("col", "UTF-8") + "=" + URLEncoder.encode(col, "UTF-8");
                data += "&" + URLEncoder.encode("like", "UTF-8") + "=" + URLEncoder.encode(like, "UTF-8");
                data += "&" + URLEncoder.encode("where", "UTF-8") + "=" + URLEncoder.encode(where, "UTF-8");
                data += "&" + URLEncoder.encode("update", "UTF-8") + "=" + URLEncoder.encode(update, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                    break;
                }

                Log.w("SQL1", sb.toString());

                return sb.toString();

            } catch (Exception e) {
                Log.e("SysSql1", e.getMessage());
                return "13rnhx87fnybwdcunx97rfgiwyg";
            }

        }if(way == 4) {

            try {

                String table = (String) arg0[0];
                String col = (String) arg0[1];
                String like = (String) arg0[2];
                String col2 = (String) arg0[3];
                String like2 = (String) arg0[4];
                String where = (String) arg0[5];
                String update = (String) arg0[6];

                String link = "http://"+f.ip+"/clogistic/dbaccess.php";

                String data  = URLEncoder.encode("way", "UTF-8") + "=" + URLEncoder.encode("update_2", "UTF-8");
                data += "&" + URLEncoder.encode("database", "UTF-8") + "=" + URLEncoder.encode("clogistic", "UTF-8");
                data += "&" + URLEncoder.encode("table", "UTF-8") + "=" + URLEncoder.encode(table, "UTF-8");
                data += "&" + URLEncoder.encode("col", "UTF-8") + "=" + URLEncoder.encode(col, "UTF-8");
                data += "&" + URLEncoder.encode("like", "UTF-8") + "=" + URLEncoder.encode(like, "UTF-8");
                data += "&" + URLEncoder.encode("col2", "UTF-8") + "=" + URLEncoder.encode(col2, "UTF-8");
                data += "&" + URLEncoder.encode("like2", "UTF-8") + "=" + URLEncoder.encode(like2, "UTF-8");

                data += "&" + URLEncoder.encode("where", "UTF-8") + "=" + URLEncoder.encode(where, "UTF-8");
                data += "&" + URLEncoder.encode("update", "UTF-8") + "=" + URLEncoder.encode(update, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                    break;
                }

                Log.w("SQL1", sb.toString());

                return sb.toString();

            } catch (Exception e) {
                Log.e("SysSql1", e.getMessage());
                return "13rnhx87fnybwdcunx97rfgiwyg";
            }

        }if(way == 5) {

            try {

                String header_no = (String) arg0[0];
                String path = (String) arg0[1];
                String usr = (String) arg0[2];


                String link = "http://"+f.ip+"/clogistic/dbaccess.php";

                String data  = URLEncoder.encode("way", "UTF-8") + "=" + URLEncoder.encode("upload_image", "UTF-8");
                data += "&" + URLEncoder.encode("database", "UTF-8") + "=" + URLEncoder.encode("clogistic", "UTF-8");
                data += "&" + URLEncoder.encode("table", "UTF-8") + "=" + URLEncoder.encode("image_upload", "UTF-8");

                data += "&" + URLEncoder.encode("header_no", "UTF-8") + "=" + URLEncoder.encode(header_no, "UTF-8");
                data += "&" + URLEncoder.encode("path", "UTF-8") + "=" + URLEncoder.encode(path, "UTF-8");
                data += "&" + URLEncoder.encode("usr", "UTF-8") + "=" + URLEncoder.encode(usr, "UTF-8");


                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                    break;
                }

                Log.w("SQL1", sb.toString());

                return sb.toString();

            } catch (Exception e) {
                Log.e("SysSql1", e.getMessage());
                return "13rnhx87fnybwdcunx97rfgiwyg";
            }

        }

        else{
            Toast.makeText(context, "Have not set any way", Toast.LENGTH_SHORT).show();
            return null;
        }

    }

    protected void onProgressUpdate(Integer... progress) {
        Log.d("SQL", "ProgressUpdate");
    }

    protected void onPostExecute(String result) {
        Log.d("SQL", "PostExecute");
        delegate.processFinish(result);
        mListener.callback();
        progressDialog.dismiss();


    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(context);

        progressDialog.setMessage("Loading Please Wait"); // message

        progressDialog.show();
    }

    public void setListener(CallBackListener listener){
        mListener = listener;
    }


}
