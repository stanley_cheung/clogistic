// ------------------------------------ DBADapter.java ---------------------------------------------

// TODO: Change the package to match your project.
package com.example.user.logistic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SysDbAdapter {

	// For logging:
	private static final String TAG = "DBAdapter";

	private final Context context;
	
	private DatabaseHelper myDBHelper;
	
	private SQLiteDatabase db;

	public static final int DATABASE_VERSION = 1;	
	
	/////////////////////////////////////////////////////////////////////
	//DATABASE 1
	/////////////////////////////////////////////////////////////////////
	public static final String DB1_ROWID = "_id";
	public static final String DB1_TYPE = "col_0";
	public static final String DB1_1 = "col_1";
	public static final String DB1_2 = "col_2";
	public static final String DB1_3 = "col_3";
	public static final String DB1_4 = "col_4";
	public static final String DB1_5 = "col_5";
	public static final String DB1_6 = "col_6";
	public static final String DB1_7 = "col_7";
	public static final String DB1_8 = "col_8";
	public static final String DB1_9 = "col_9";
	public static final String DB1_10 = "col_10";
	public static final String DB1_11 = "col_11";
	public static final String DB1_12 = "col_12";
	public static final String DB1_13 = "col_13";
	public static final String DB1_14 = "col_14";
	public static final String DB1_15 = "col_15";
	public static final String DB1_16 = "col_16";
	public static final String DB1_17 = "col_17";
	public static final String DB1_18 = "col_18";
	public static final String DB1_19 = "col_19";
	public static final String DB1_20 = "col_20";


	public static final String[] ALL_DB1_KEYS = new String[] {DB1_ROWID, DB1_TYPE, DB1_1, DB1_2, DB1_3, DB1_4, DB1_5, DB1_6, DB1_7, DB1_8, DB1_9, DB1_10, DB1_11, DB1_12, DB1_13, DB1_14, DB1_15, DB1_16, DB1_17, DB1_18, DB1_19, DB1_20};

	public static final String DB1_NAME = "db1_name";
	public static final String DB1_TABLE = "db1_table";

	private static final String DATABASE1_CREATE_SQL =
		"create table if not exists " + DB1_TABLE 
		+ " (" + DB1_ROWID + " integer primary key autoincrement, "
		+ DB1_TYPE + " string not null, "
		+ DB1_1 + " string not null, "
		+ DB1_2 + " string not null, "
		+ DB1_3 + " text not null, "
		+ DB1_4 + " text not null, "
		+ DB1_5 + " int not null, "		
		+ DB1_6 + " int not null, "
		+ DB1_7 + " int not null, "
		+ DB1_8 + " double not null, "
		+ DB1_9 + " double not null, "
		+ DB1_10 + " double not null, "
		+ DB1_11 + " string not null, "
		+ DB1_12 + " string not null, "
		+ DB1_13 + " text not null, "
		+ DB1_14 + " text not null, "
		+ DB1_15 + " text not null, "
		+ DB1_16 + " text not null, "
		+ DB1_17 + " text not null, "
		+ DB1_18 + " double not null, "
		+ DB1_19 + " double not null, "
		+ DB1_20 + " double not null "
		+ ");";



	/////////////////////////////////////////////////////////////////////
	//DATABASE 2
	/////////////////////////////////////////////////////////////////////
	public static final String DB2_ROWID = "_id";
	public static final String DB2_TYPE = "col_0";
	public static final String DB2_1 = "col_1";
	public static final String DB2_2 = "col_2";
	public static final String DB2_3 = "col_3";
	public static final String DB2_4 = "col_4";
	public static final String DB2_5 = "col_5";
	public static final String DB2_6 = "col_6";
	public static final String DB2_7 = "col_7";
	public static final String DB2_8 = "col_8";
	public static final String DB2_9 = "col_9";
	public static final String DB2_10 = "col_10";
	public static final String DB2_11 = "col_11";
	public static final String DB2_12 = "col_12";
	public static final String DB2_13 = "col_13";
	public static final String DB2_14 = "col_14";
	public static final String DB2_15 = "col_15";
	public static final String DB2_16 = "col_16";
	public static final String DB2_17 = "col_17";
	public static final String DB2_18 = "col_18";
	public static final String DB2_19 = "col_19";
	public static final String DB2_20 = "col_20";

	public static final String DB2_TABLE = "db2_table";

	private static final String DATABASE2_CREATE_SQL =
		"create table if not exists " + DB2_TABLE 
		+ " (" + DB2_ROWID + " integer primary key autoincrement, "
		+ DB2_TYPE + " string not null, "
		+ DB2_1 + " string not null, "
		+ DB2_2 + " string not null, "
		+ DB2_3 + " text not null, "
		+ DB2_4 + " text not null, "
		+ DB2_5 + " int not null, "		
		+ DB2_6 + " int not null, "
		+ DB2_7 + " int not null, "
		+ DB2_8 + " double not null, "
		+ DB2_9 + " double not null, "
		+ DB2_10 + " double not null, "
		+ DB2_11 + " string not null, "
		+ DB2_12 + " string not null, "
		+ DB2_13 + " text not null, "
		+ DB2_14 + " text not null, "
		+ DB2_15 + " text not null, "
		+ DB2_16 + " text not null, "
		+ DB2_17 + " text not null, "
		+ DB2_18 + " double not null, "
		+ DB2_19 + " double not null, "
		+ DB2_20 + " double not null "
		+ ");";

	/////////////////////////////////////////////////////////////////////
	//DATABASE 3
	/////////////////////////////////////////////////////////////////////
	public static final String DB3_ROWID = "_id";
	public static final String DB3_TYPE = "col_0";
	public static final String DB3_1 = "col_1";
	public static final String DB3_2 = "col_2";
	public static final String DB3_3 = "col_3";
	public static final String DB3_4 = "col_4";
	public static final String DB3_5 = "col_5";
	public static final String DB3_6 = "col_6";
	public static final String DB3_7 = "col_7";
	public static final String DB3_8 = "col_8";
	public static final String DB3_9 = "col_9";
	public static final String DB3_10 = "col_10";
	public static final String DB3_11 = "col_11";
	public static final String DB3_12 = "col_12";
	public static final String DB3_13 = "col_13";
	public static final String DB3_14 = "col_14";
	public static final String DB3_15 = "col_15";
	public static final String DB3_16 = "col_16";
	public static final String DB3_17 = "col_17";
	public static final String DB3_18 = "col_18";
	public static final String DB3_19 = "col_19";
	public static final String DB3_20 = "col_20";

	public static final String DB3_TABLE = "db3_table";

	private static final String DATABASE3_CREATE_SQL =
		"create table if not exists " + DB3_TABLE 
		+ " (" + DB3_ROWID + " integer primary key autoincrement, "
		+ DB3_TYPE + " string not null, "
		+ DB3_1 + " string not null, "
		+ DB3_2 + " string not null, "
		+ DB3_3 + " text not null, "
		+ DB3_4 + " text not null, "
		+ DB3_5 + " int not null, "		
		+ DB3_6 + " int not null, "
		+ DB3_7 + " int not null, "
		+ DB3_8 + " double not null, "
		+ DB3_9 + " double not null, "
		+ DB3_10 + " double not null, "
		+ DB3_11 + " string not null, "
		+ DB3_12 + " string not null, "
		+ DB3_13 + " text not null, "
		+ DB3_14 + " text not null, "
		+ DB3_15 + " text not null, "
		+ DB3_16 + " text not null, "
		+ DB3_17 + " text not null, "
		+ DB3_18 + " double not null, "
		+ DB3_19 + " double not null, "
		+ DB3_20 + " double not null "
		+ ");";
	
	/////////////////////////////////////////////////////////////////////
	//DATABASE 4
	/////////////////////////////////////////////////////////////////////
	public static final String DB4_ROWID = "_id";
	public static final String DB4_TYPE = "col_0";
	public static final String DB4_1 = "col_1";
	public static final String DB4_2 = "col_2";
	public static final String DB4_3 = "col_3";
	public static final String DB4_4 = "col_4";
	public static final String DB4_5 = "col_5";
	public static final String DB4_6 = "col_6";
	public static final String DB4_7 = "col_7";
	public static final String DB4_8 = "col_8";
	public static final String DB4_9 = "col_9";
	public static final String DB4_10 = "col_10";
	public static final String DB4_11 = "col_11";
	public static final String DB4_12 = "col_12";
	public static final String DB4_13 = "col_13";
	public static final String DB4_14 = "col_14";
	public static final String DB4_15 = "col_15";
	public static final String DB4_16 = "col_16";
	public static final String DB4_17 = "col_17";
	public static final String DB4_18 = "col_18";
	public static final String DB4_19 = "col_19";
	public static final String DB4_20 = "col_20";

	public static final String DB4_TABLE = "db4_table";

	private static final String DATABASE4_CREATE_SQL =
		"create table if not exists " + DB4_TABLE 
		+ " (" + DB4_ROWID + " integer primary key autoincrement, "
		+ DB4_TYPE + " string not null, "
		+ DB4_1 + " string not null, "
		+ DB4_2 + " string not null, "
		+ DB4_3 + " text not null, "
		+ DB4_4 + " text not null, "
		+ DB4_5 + " int not null, "		
		+ DB4_6 + " int not null, "
		+ DB4_7 + " int not null, "
		+ DB4_8 + " double not null, "
		+ DB4_9 + " double not null, "
		+ DB4_10 + " double not null, "
		+ DB4_11 + " string not null, "
		+ DB4_12 + " string not null, "
		+ DB4_13 + " text not null, "
		+ DB4_14 + " text not null, "
		+ DB4_15 + " text not null, "
		+ DB4_16 + " text not null, "
		+ DB4_17 + " text not null, "
		+ DB4_18 + " double not null, "
		+ DB4_19 + " double not null, "
		+ DB4_20 + " double not null "
		+ ");";

	/////////////////////////////////////////////////////////////////////
	//DATABASE 5
	/////////////////////////////////////////////////////////////////////
	public static final String DB5_ROWID = "_id";
	public static final String DB5_TYPE = "col_0";
	public static final String DB5_1 = "col_1";
	public static final String DB5_2 = "col_2";
	public static final String DB5_3 = "col_3";
	public static final String DB5_4 = "col_4";
	public static final String DB5_5 = "col_5";
	public static final String DB5_6 = "col_6";
	public static final String DB5_7 = "col_7";
	public static final String DB5_8 = "col_8";
	public static final String DB5_9 = "col_9";
	public static final String DB5_10 = "col_10";
	public static final String DB5_11 = "col_11";
	public static final String DB5_12 = "col_12";
	public static final String DB5_13 = "col_13";
	public static final String DB5_14 = "col_14";
	public static final String DB5_15 = "col_15";
	public static final String DB5_16 = "col_16";
	public static final String DB5_17 = "col_17";
	public static final String DB5_18 = "col_18";
	public static final String DB5_19 = "col_19";
	public static final String DB5_20 = "col_20";

	public static final String DB5_TABLE = "db5_table";

	private static final String DATABASE5_CREATE_SQL =
			"create table if not exists " + DB5_TABLE
					+ " (" + DB5_ROWID + " integer primary key autoincrement, "
					+ DB5_TYPE + " string not null, "
					+ DB5_1 + " string not null, "
					+ DB5_2 + " string not null, "
					+ DB5_3 + " text not null, "
					+ DB5_4 + " text not null, "
					+ DB5_5 + " int not null, "
					+ DB5_6 + " int not null, "
					+ DB5_7 + " int not null, "
					+ DB5_8 + " double not null, "
					+ DB5_9 + " double not null, "
					+ DB5_10 + " double not null, "
					+ DB5_11 + " string not null, "
					+ DB5_12 + " string not null, "
					+ DB5_13 + " text not null, "
					+ DB5_14 + " text not null, "
					+ DB5_15 + " text not null, "
					+ DB5_16 + " text not null, "
					+ DB5_17 + " text not null, "
					+ DB5_18 + " double not null, "
					+ DB5_19 + " double not null, "
					+ DB5_20 + " double not null "
					+ ");";

	/////////////////////////////////////////////////////////////////////
	//DATABASE 6
	/////////////////////////////////////////////////////////////////////
	public static final String DB6_ROWID = "_id";
	public static final String DB6_TYPE = "col_0";
	public static final String DB6_1 = "col_1";
	public static final String DB6_2 = "col_2";
	public static final String DB6_3 = "col_3";
	public static final String DB6_4 = "col_4";
	public static final String DB6_5 = "col_5";
	public static final String DB6_6 = "col_6";
	public static final String DB6_7 = "col_7";
	public static final String DB6_8 = "col_8";
	public static final String DB6_9 = "col_9";
	public static final String DB6_10 = "col_10";
	public static final String DB6_11 = "col_11";
	public static final String DB6_12 = "col_12";
	public static final String DB6_13 = "col_13";
	public static final String DB6_14 = "col_14";
	public static final String DB6_15 = "col_15";
	public static final String DB6_16 = "col_16";
	public static final String DB6_17 = "col_17";
	public static final String DB6_18 = "col_18";
	public static final String DB6_19 = "col_19";
	public static final String DB6_20 = "col_20";
	public static final String DB6_21 = "col_21";
	public static final String DB6_22 = "col_22";
	public static final String DB6_23 = "col_23";
	public static final String DB6_24 = "col_24";
	public static final String DB6_25 = "col_25";
	public static final String DB6_26 = "col_26";
	public static final String DB6_27 = "col_27";
	public static final String DB6_28 = "col_28";
	public static final String DB6_29 = "col_29";

	public static final String DB6_TABLE = "db6_table";

	private static final String DATABASE6_CREATE_SQL =
			"create table if not exists " + DB6_TABLE
					+ " (" + DB6_ROWID + " integer primary key autoincrement, "
					+ DB6_TYPE + " string not null, "
					+ DB6_1 + " string not null, "
					+ DB6_2 + " int not null, "
					+ DB6_3 + " int not null, "
					+ DB6_4 + " int not null, "
					+ DB6_5 + " string not null, "
					+ DB6_6 + " string not null, "
					+ DB6_7 + " string not null, "
					+ DB6_8 + " int not null, "
					+ DB6_9 + " int not null, "
					+ DB6_10 + " int not null, "
					+ DB6_11 + " int not null, "
					+ DB6_12 + " int not null, "
					+ DB6_13 + " int not null, "
					+ DB6_14 + " string not null, "
					+ DB6_15 + " int not null, "
					+ DB6_16 + " int not null, "
					+ DB6_17 + " int not null, "
					+ DB6_18 + " double not null, "
					+ DB6_19 + " double not null, "
					+ DB6_20 + " double not null, "
					+ DB6_21 + " double not null, "
					+ DB6_22 + " string not null, "
					+ DB6_23 + " string not null, "
					+ DB6_24 + " string not null, "
					+ DB6_25 + " string not null, "
					+ DB6_26 + " string not null, "
					+ DB6_27 + " string not null, "
					+ DB6_28 + " double not null, "
					+ DB6_29 + " string not null "
					+ ");";

	/////////////////////////////////////////////////////////////////////
	//DATABASE 7
	/////////////////////////////////////////////////////////////////////
	public static final String DB7_ROWID = "_id";
	public static final String DB7_TYPE = "col_0";
	public static final String DB7_1 = "col_1";
	public static final String DB7_2 = "col_2";
	public static final String DB7_3 = "col_3";
	public static final String DB7_4 = "col_4";
	public static final String DB7_5 = "col_5";
	public static final String DB7_6 = "col_6";
	public static final String DB7_7 = "col_7";
	public static final String DB7_8 = "col_8";
	public static final String DB7_9 = "col_9";
	public static final String DB7_10 = "col_10";
	public static final String DB7_11 = "col_11";
	public static final String DB7_12 = "col_12";
	public static final String DB7_13 = "col_13";
	public static final String DB7_14 = "col_14";
	public static final String DB7_15 = "col_15";
	public static final String DB7_16 = "col_16";
	public static final String DB7_17 = "col_17";
	public static final String DB7_18 = "col_18";
	public static final String DB7_19 = "col_19";
	public static final String DB7_20 = "col_20";

	public static final String DB7_TABLE = "db7_table";

	private static final String DATABASE7_CREATE_SQL =
			"create table if not exists " + DB7_TABLE
					+ " (" + DB7_ROWID + " integer primary key autoincrement, "
					+ DB7_TYPE + " string not null, "
					+ DB7_1 + " string not null, "
					+ DB7_2 + " string not null, "
					+ DB7_3 + " text not null, "
					+ DB7_4 + " text not null, "
					+ DB7_5 + " int not null, "
					+ DB7_6 + " int not null, "
					+ DB7_7 + " int not null, "
					+ DB7_8 + " double not null, "
					+ DB7_9 + " double not null, "
					+ DB7_10 + " double not null, "
					+ DB7_11 + " string not null, "
					+ DB7_12 + " string not null, "
					+ DB7_13 + " text not null, "
					+ DB7_14 + " text not null, "
					+ DB7_15 + " text not null, "
					+ DB7_16 + " text not null, "
					+ DB7_17 + " text not null, "
					+ DB7_18 + " double not null, "
					+ DB7_19 + " double not null, "
					+ DB7_20 + " double not null "
					+ ");";


	/////////////////////////////////////////////////////////////////////
	//DATABASE 8
	/////////////////////////////////////////////////////////////////////
	public static final String DB8_ROWID = "_id";
	public static final String DB8_TYPE = "col_0";
	public static final String DB8_1 = "col_1";
	public static final String DB8_2 = "col_2";
	public static final String DB8_3 = "col_3";
	public static final String DB8_4 = "col_4";
	public static final String DB8_5 = "col_5";
	public static final String DB8_6 = "col_6";
	public static final String DB8_7 = "col_7";
	public static final String DB8_8 = "col_8";
	public static final String DB8_9 = "col_9";
	public static final String DB8_10 = "col_10";
	public static final String DB8_11 = "col_11";
	public static final String DB8_12 = "col_12";
	public static final String DB8_13 = "col_13";
	public static final String DB8_14 = "col_14";
	public static final String DB8_15 = "col_15";
	public static final String DB8_16 = "col_16";
	public static final String DB8_17 = "col_17";
	public static final String DB8_18 = "col_18";
	public static final String DB8_19 = "col_19";
	public static final String DB8_20 = "col_20";

	public static final String DB8_TABLE = "db8_table";

	private static final String DATABASE8_CREATE_SQL =
			"create table if not exists " + DB8_TABLE
					+ " (" + DB8_ROWID + " integer primary key autoincrement, "
					+ DB8_TYPE + " string not null, "
					+ DB8_1 + " string not null, "
					+ DB8_2 + " string not null, "
					+ DB8_3 + " text not null, "
					+ DB8_4 + " text not null, "
					+ DB8_5 + " int not null, "
					+ DB8_6 + " int not null, "
					+ DB8_7 + " int not null, "
					+ DB8_8 + " double not null, "
					+ DB8_9 + " double not null, "
					+ DB8_10 + " double not null, "
					+ DB8_11 + " string not null, "
					+ DB8_12 + " string not null, "
					+ DB8_13 + " text not null, "
					+ DB8_14 + " text not null, "
					+ DB8_15 + " text not null, "
					+ DB8_16 + " text not null, "
					+ DB8_17 + " text not null, "
					+ DB8_18 + " double not null, "
					+ DB8_19 + " double not null, "
					+ DB8_20 + " double not null "
					+ ");";


	/////////////////////////////////////////////////////////////////////
	//	Public methods:
	/////////////////////////////////////////////////////////////////////
	
	public SysDbAdapter(Context ctx) {
		this.context = ctx;
		myDBHelper = new DatabaseHelper(context);
	}

	public SysDbAdapter open() {
		db = myDBHelper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		myDBHelper.close();
	}
	
	public void Create_DB1(Context context){
		db.execSQL(DATABASE1_CREATE_SQL);
	};
	public void Create_DB2(Context context){
		db.execSQL(DATABASE2_CREATE_SQL);
	};
	public void Create_DB3(Context context){
		db.execSQL(DATABASE3_CREATE_SQL);
	};
	public void Create_DB4(Context context){
		db.execSQL(DATABASE4_CREATE_SQL);
	};
	public void Create_DB5(Context context){
		db.execSQL(DATABASE5_CREATE_SQL);
	};
	public void Create_DB6(Context context){
		db.execSQL(DATABASE6_CREATE_SQL);
	};
	public void Create_DB7(Context context){
		db.execSQL(DATABASE7_CREATE_SQL);
	};
	public void Create_DB8(Context context){
		db.execSQL(DATABASE8_CREATE_SQL);
	};



	public void droptable(String table){
		db.execSQL("DROP TABLE IF EXISTS " + table);
		Log.d(TAG, "table droped");
	};

	public long InsertData(String table, String type, String data_1, String data_2, String data_3, String data_4, int data_5, int data_6, int data_7, double data_8, double data_9, double data_10, String data_11, String data_12, String data_13, String data_14, int data_15, int data_16, int data_17, double data_18, double data_19, double data_20) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(DB4_TYPE, type);
		initialValues.put(DB4_1, data_1);
		initialValues.put(DB4_2, data_2);
		initialValues.put(DB4_3, data_3);
		initialValues.put(DB4_4, data_4);
		initialValues.put(DB4_5, data_5);
		initialValues.put(DB4_6, data_6);
		initialValues.put(DB4_7, data_7);
		initialValues.put(DB4_8, data_8);
		initialValues.put(DB4_9, data_9);
		initialValues.put(DB4_10, data_10);
		initialValues.put(DB4_11, data_11);
		initialValues.put(DB4_12, data_12);
		initialValues.put(DB4_13, data_13);
		initialValues.put(DB4_14, data_14);
		initialValues.put(DB4_15, data_15);
		initialValues.put(DB4_16, data_16);
		initialValues.put(DB4_17, data_17);
		initialValues.put(DB4_18, data_18);
		initialValues.put(DB4_19, data_19);
		initialValues.put(DB4_20, data_20);

		return db.insert(table, null, initialValues);
	}

	public long InsertData_big(String table, String type, String data_1, int data_2, int data_3, int data_4, String data_5, String data_6, String data_7, int data_8, int data_9, int data_10, int data_11, int data_12, int data_13, String data_14, int data_15, int data_16, int data_17, double data_18, double data_19, double data_20, double data_21, String data_22, String data_23, String data_24, String data_25, String data_26, String data_27, String data_28, String data_29) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(DB6_TYPE, type);
		initialValues.put(DB6_1, data_1);
		initialValues.put(DB6_2, data_2);
		initialValues.put(DB6_3, data_3);
		initialValues.put(DB6_4, data_4);
		initialValues.put(DB6_5, data_5);
		initialValues.put(DB6_6, data_6);
		initialValues.put(DB6_7, data_7);
		initialValues.put(DB6_8, data_8);
		initialValues.put(DB6_9, data_9);
		initialValues.put(DB6_10, data_10);
		initialValues.put(DB6_11, data_11);
		initialValues.put(DB6_12, data_12);
		initialValues.put(DB6_13, data_13);
		initialValues.put(DB6_14, data_14);
		initialValues.put(DB6_15, data_15);
		initialValues.put(DB6_16, data_16);
		initialValues.put(DB6_17, data_17);
		initialValues.put(DB6_18, data_18);
		initialValues.put(DB6_19, data_19);
		initialValues.put(DB6_20, data_20);
		initialValues.put(DB6_21, data_21);
		initialValues.put(DB6_22, data_22);
		initialValues.put(DB6_23, data_23);
		initialValues.put(DB6_24, data_24);
		initialValues.put(DB6_25, data_25);
		initialValues.put(DB6_26, data_26);
		initialValues.put(DB6_27, data_27);
		initialValues.put(DB6_28, data_28);
		initialValues.put(DB6_29, data_29);

		return db.insert(table, null, initialValues);
	}

	public Cursor GetAllRow(String table) {
		String where = null;
		Cursor c = 	db.query(true, table, ALL_DB1_KEYS,
							where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}

	public Cursor GetDateTimeAs(String table, String where, String like){
		return  db.rawQuery(

				"SELECT * FROM " +table+ " WHERE " + where + " LIKE '%" + like + "%'" + " ORDER BY `col_6`"
				, null);

	}

	public Cursor GetDistrictAs(String table, String table2, String where, String like){
		return  db.rawQuery(
				"SELECT db1.* FROM " +table+ " AS db1 JOIN " +table2+ " AS db2 ON db2.`col_0` = db1.`col_1` WHERE db1." + where + " LIKE '%" + like + "%'" + " ORDER BY db2.`col_2` ASC"
				, null);

	}
	
	public Cursor GetSpecificItem(String table , String where, String like){
			return  db.rawQuery(
	    		   
	    		   "SELECT * FROM " +table+ " WHERE " +
	    			where+ " LIKE '%" +like+ "%'"
	    		   , null);
	       
	    }

	public Cursor GetSpecificItem_3i(String table , String where1, String like1, String where2, String like2, String where3, String like3){
		return  db.rawQuery(

				"SELECT * FROM " +table+ " WHERE " +
						where1+ " LIKE '%" +like1+ "%'" + " AND " +
						where2+ " LIKE '%" +like2+ "%'" + " AND " +
						where3+ " LIKE '%" +like3+ "%'"
				, null);

	}

	public Cursor GetRangeWithString( String table , String greater, int greater_no, String smaller, int smaller_no, String col, String like){
		Log.d("GetRangeWithString", "SELECT * FROM " + table + " WHERE " +
				greater + " >= " + greater_no + " AND " + smaller + " <= " + smaller_no + " AND " + col + " LIKE '%" + like + "%'");

		return  db.rawQuery(

				"SELECT * FROM " +table+ " WHERE " +
						greater + " >= " + greater_no + " AND " + smaller + " <= " + smaller_no + " AND " + col + " LIKE '%" + like + "%'"
				, null);

	}

	public Cursor GetRange( String table , String greater, int greater_no, String smaller, int smaller_no){
		Log.d("GetRange", "SELECT * FROM " + table + " WHERE " +
				greater + " >= " + greater_no + " AND " + smaller + " =< " + smaller_no);

		return  db.rawQuery(

				"SELECT * FROM " +table+ " WHERE cast(" +
						greater + " as INTEGER)" + " >= " + greater_no + " AND cast(" + smaller + " as INTEGER)" + " <= " + smaller_no
				, null);

	}

	public boolean UpdateRow(String table, String col, long like, String type, String data_1, String data_2, String data_3, String data_4, int data_5, int data_6, int data_7, double data_8, double data_9, double data_10, String data_11, String data_12, String data_13, String data_14, int data_15, int data_16, int data_17, double data_18, double data_19, double data_20) {
		String where = col + "=" + like;

		ContentValues newValues = new ContentValues();

		newValues.put(DB4_TYPE, type);
		newValues.put(DB4_1, data_1);
		newValues.put(DB4_2, data_2);
		newValues.put(DB4_3, data_3);
		newValues.put(DB4_4, data_4);
		newValues.put(DB4_5, data_5);
		newValues.put(DB4_6, data_6);
		newValues.put(DB4_7, data_7);
		newValues.put(DB4_8, data_8);
		newValues.put(DB4_9, data_9);
		newValues.put(DB4_10, data_10);
		newValues.put(DB4_11, data_11);
		newValues.put(DB4_12, data_12);
		newValues.put(DB4_13, data_13);
		newValues.put(DB4_14, data_14);
		newValues.put(DB4_15, data_15);
		newValues.put(DB4_16, data_16);
		newValues.put(DB4_17, data_17);
		newValues.put(DB4_18, data_18);
		newValues.put(DB4_19, data_19);
		newValues.put(DB4_20, data_20);

		return db.update(table, newValues, where, null) != 0;
	}

	public boolean UpdateItem_s(String table, String col, String like, String up_col, String data_1) {
		String where = col + " LIKE '%" +like+ "%'";

		ContentValues newValues = new ContentValues();
		newValues.put(up_col, data_1);
		return db.update(table, newValues, where, null) != 0;
	}

	public boolean UpdateItem_i(String table, String col, String like, String up_col, int data_1) {
		String where = col + " LIKE '%" +like+ "%'";

		ContentValues newValues = new ContentValues();
		newValues.put(up_col, data_1);
		return db.update(table, newValues, where, null) != 0;
	}

	public boolean UpdateItem_d(String table, String col, String like, String up_col, double data_1) {
		String where = col + " LIKE '%" +like+ "%'";

		ContentValues newValues = new ContentValues();
		newValues.put(up_col, data_1);
		return db.update(table, newValues, where, null) != 0;
	}

	public boolean Delete_row(String table, String col, String like) {
		String where = col + " LIKE '%" +like+ "%'";
		return db.delete(table, where, null) != 0;
	}

	public void Delete_All(String table) {
		Cursor c = GetAllRow(table);
		long rowId = c.getColumnIndexOrThrow(DB1_ROWID);
		if (c.moveToFirst()) {
			do {
				Delete_row(table, "_id", Long.toString(c.getLong((int) rowId)));
			} while (c.moveToNext());
		}
		c.close();
	}

	/*
	public boolean Update_Data_Row(long rowId, String type, String data_1, String data_2, int data_3, String data_4, int data_5) {
		String where = DATA_ROWID + "=" + rowId;

		ContentValues newValues = new ContentValues();
		newValues.put(DATA_TYPE, type);
		newValues.put(DATA_1, data_1);
		newValues.put(DATA_2, data_2);
		newValues.put(DATA_3, data_3);
		newValues.put(DATA_4, data_4);
		newValues.put(DATA_4, data_5);

		return db.update(DATA_TABLE, newValues, where, null) != 0;
	}

	public long Insert_RP_Row(int no, String date, String code, String item, double qty, double amount, double avgp) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(RP_NO, no);
		initialValues.put(RP_DATE, date);
		initialValues.put(RP_CODE, code);
		initialValues.put(RP_ITEM, item);
		initialValues.put(RP_QTY, qty);
		initialValues.put(RP_AMOUNT, amount);
		initialValues.put(RP_AVGP, avgp);
		
		return db.insert(RP_TABLE, null, initialValues);
	}

	public boolean Update_RP_Row(long rowId, int no, String date, String code, String item, double qty, double amount, double avgp) {
		String where = RP_ROWID + "=" + rowId;

		ContentValues newValues = new ContentValues();

		newValues.put(RP_NO, no);
		newValues.put(RP_DATE, date);
		newValues.put(RP_CODE, code);
		newValues.put(RP_ITEM, item);
		newValues.put(RP_QTY, qty);
		newValues.put(RP_AMOUNT, amount);
		newValues.put(RP_AVGP, avgp);

		return db.update(RP_TABLE, newValues, where, null) != 0;
	}

	public long Insert_PD_Row(int code, String name, String gender, String cat, String color, double price, String brand, String size, int photo, String detail) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(PD_CODE, code);
		initialValues.put(PD_NAME, name);
		initialValues.put(PD_GENDER, gender);
		initialValues.put(PD_CAT, cat);
		initialValues.put(PD_COLOR, color);
		initialValues.put(PD_PRICE, price);
		initialValues.put(PD_BRAND, brand);
		initialValues.put(PD_SIZE, size);
		initialValues.put(PD_PHOTO, photo);
		initialValues.put(PD_DETAIL, detail);

		return db.insert(PD_TABLE, null, initialValues);
	}

	public boolean Update_PD_Row(long rowId, int code, String name, String gender, String cat, String color, double price, String brand, String size, int photo, String detail) {
		String where = PD_ROWID + "=" + rowId;

		ContentValues newValues = new ContentValues();

		newValues.put(PD_CODE, code);
		newValues.put(PD_NAME, name);
		newValues.put(PD_GENDER, gender);
		newValues.put(PD_CAT, cat);
		newValues.put(PD_COLOR, color);
		newValues.put(PD_PRICE, price);
		newValues.put(PD_BRAND, brand);
		newValues.put(PD_SIZE, size);
		newValues.put(PD_PHOTO, photo);
		newValues.put(PD_DETAIL, detail);

		return db.update(PD_TABLE, newValues, where, null) != 0;
	}

	public long Insert_CL_Row(int code, String name, long date, String com, String add, String title, long offno, long mobno, String email, int cusgp, String rm) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(CL_CODE, code);
		initialValues.put(CL_NAME, name);
		initialValues.put(CL_CREDATE, date);
		initialValues.put(CL_COM, com);
		initialValues.put(CL_ADD, add);
		initialValues.put(CL_TITLE, title);
		initialValues.put(CL_OFFNO, offno);
		initialValues.put(CL_MOBNO, mobno);
		initialValues.put(CL_EMAIL, email);
		initialValues.put(CL_CUSGP, cusgp);
		initialValues.put(CL_RM, rm);
		
		return db.insert(CL_TABLE, null, initialValues);
	}
	
	public boolean Update_CL_Row(long rowId, String name, String com, String add, String title, long offno, long mobno, String email, int cusgp, String rm) {
		String where = CL_ROWID + "=" + rowId;

		ContentValues newValues = new ContentValues();

		newValues.put(CL_NAME, name);
		newValues.put(CL_COM, com);
		newValues.put(CL_ADD, add);
		newValues.put(CL_TITLE, title);
		newValues.put(CL_OFFNO, offno);
		newValues.put(CL_MOBNO, mobno);
		newValues.put(CL_EMAIL, email);
		newValues.put(CL_CUSGP, cusgp);
		newValues.put(CL_RM, rm);

		return db.update(CL_TABLE, newValues, where, null) != 0;
	}
	
	public Cursor data_getAllRows() {
		String where = null;
		Cursor c = 	db.query(true, DATA_TABLE, ALL_DATA_KEYS, 
							where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public Cursor rp_getAllRows() {
		String where = null;
		Cursor c = 	db.query(true, RP_TABLE, ALL_RP_KEYS, 
							where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public Cursor pd_getAllRows() {
		String where = null;
		Cursor c = 	db.query(true, PD_TABLE, ALL_PD_KEYS, 
							where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public Cursor cl_getAllRows() {
		String where = null;
		Cursor c = 	db.query(true, CL_TABLE, ALL_CL_KEYS, 
							where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public void data_deleteAll() {
		Cursor c = data_getAllRows();
		long rowId = c.getColumnIndexOrThrow(DATA_ROWID);
		if (c.moveToFirst()) {
			do {
				data_deleteRow(c.getLong((int) rowId));				
			} while (c.moveToNext());
		}
		c.close();
	}
	

	
	public void rp_deleteAll() {
		Cursor c = rp_getAllRows();
		long rowId = c.getColumnIndexOrThrow(RP_ROWID);
		if (c.moveToFirst()) {
			do {
				rp_deleteRow(c.getLong((int) rowId));				
			} while (c.moveToNext());
		}
		c.close();
	}
	
	public boolean rp_deleteRow(long rowId) {
		String where = RP_ROWID + "=" + rowId;
		return db.delete(RP_TABLE, where, null) != 0;
	}
	
	public void pd_deleteAll() {
		Cursor c = pd_getAllRows();
		long rowId = c.getColumnIndexOrThrow(PD_ROWID);
		if (c.moveToFirst()) {
			do {
				pd_deleteRow(c.getLong((int) rowId));				
			} while (c.moveToNext());
		}
		c.close();
	}
	
	public boolean pd_deleteRow(long rowId) {
		String where = PD_ROWID + "=" + rowId;
		return db.delete(PD_TABLE, where, null) != 0;
	}
	
	public boolean cl_deleteRow(long rowId) {
		String where = CL_ROWID + "=" + rowId;
		return db.delete(CL_TABLE, where, null) != 0;
	}
	
	
	public  Cursor getSepficItem_discount(String data_type){
		String table = DATA_TABLE;   
		return  db.rawQuery(
    		   
    		   "SELECT * FROM " +table+ " WHERE " +
    		   DATA_1+ " LIKE '%" +data_type+ "%'"
    		   , null);
       
    }
	
	public  Cursor getSepficItem_pd(String gender, String cat, String color){
		   String table = PD_TABLE;
	       return  db.rawQuery(
	    		   
	    		   "SELECT * FROM " +table+ " WHERE " +
	    		   PD_GENDER+ " LIKE '%" +gender+ "%'" +
	    		   " AND "+
	    		   PD_CAT+ " LIKE '%" +cat+ "%'" +
	    		   " AND "+
	    		   PD_COLOR+ " LIKE '%" +color+ "%'"
	    		   
	    		   , null);
	       
	    }
	
	public Cursor getSepficItem_bill(long bill) {
		String where = RP_NO + "=" + bill;
		Cursor c = 	db.query(true, RP_TABLE, ALL_RP_KEYS, 
						where, null, null, null, null, null);
		
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}	

	public  Cursor rp_getBy_date(String date){
		String table = RP_TABLE;   
		return  db.rawQuery(
    		   
	    		"SELECT * FROM " +table+ " WHERE " +
	    		RP_DATE + " LIKE '%" +date+ "%'"
			    , null);

       
    }
	
	public Cursor pd_getRow(long rowId) {
		String where = PD_CODE + "=" + rowId;
		Cursor c = 	db.query(true, PD_TABLE, ALL_PD_KEYS, 
						where, null, null, null, null, null);
		
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public Cursor rp_getRow(long rowId) {
		String where = RP_ROWID + "=" + rowId;
		Cursor c = 	db.query(true, RP_TABLE, ALL_RP_KEYS, 
						where, null, null, null, null, null);
		
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public Cursor cl_getRow(long rowId) {
		String where = CL_ROWID + "=" + rowId;
		Cursor c = 	db.query(true, CL_TABLE, ALL_CL_KEYS, 
						where, null, null, null, null, null);
		
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}

	public Cursor rp_getReceiptNo(long ReceiptNo) {
		String where = RP_NO + "=" + ReceiptNo;
		Cursor c = 	db.query(true, RP_TABLE, ALL_RP_KEYS, 
						where, null, null, null, null, null);
		
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public boolean delete_dataType(String DeleteItem) {
		return db.delete(DATA_TABLE, DATA_TYPE + " LIKE '%" + DeleteItem + "%'", null) != 0;
	}
	
	public boolean delete_rp_Row(long bill) {
		String where = RP_NO + "=" + bill;
		return db.delete(RP_TABLE, where, null) != 0;
	}

	
	*/
	/////////////////////////////////////////////////////////////////////
	//	Private Helper Classes:
	/////////////////////////////////////////////////////////////////////

	private static class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context) {
			super(context, DB1_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase _db) {
			_db.execSQL(DATABASE1_CREATE_SQL);			
		}

		@Override
		public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading application's database from version " + oldVersion
					+ " to " + newVersion + ", which will destroy all old data!");
			
			// Destroy old database:
			_db.execSQL("DROP TABLE IF EXISTS " + DB1_TABLE);
			
			// Recreate new database:
			onCreate(_db);
		}
	}
}
