package com.example.user.logistic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by user on 18/8/2015.
 */
public class PageConfirm extends AppCompatActivity implements AsyncResponse{

    public static final int CAPTURE_IMAGE_THUMBNAIL_ACTIVITY_REQUEST_CODE = 1888;
    public static final int CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE = 1777;

    Toolbar toolbar;

    String grp_id;

    ImageButton cam_btn_1;
    Button update_btn;
    Button close_btn;

    ImageButton img_cap;
    ImageButton img_cap2;
    ImageButton img_cap3;
    ImageButton img_cap4;

    String link1;
    String link2;
    String link3;
    String link4;

    String do_no;

    TextView company;
    TextView adrs;
    TextView tel;
    TextView time;

    //Database use
    String return_pass;
    SysDbAdapter dba = new SysDbAdapter(this);
    F f = new F();

    //Product listview use
    private SimpleAdapter adapter1;
    ArrayList<HashMap<String, String>> list2 = new ArrayList<HashMap<String, String>>();
    int stock_count;
    int less_count;
    int damage_count;
    int total_count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.page_confirm);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        dba = new SysDbAdapter(PageConfirm.this);


        grp_id = getIntent().getStringExtra("dono");

        final File file1 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail1.jpg");
        final File file2 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail2.jpg");
        final File file3 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail3.jpg");
        final File file4 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail4.jpg");

        //Get DO Number
        Intent i = getIntent();
        do_no = i.getStringExtra("dono");

        //UploadFile2(file1.getPath(), do_no);

        img_cap = (ImageButton)findViewById(R.id.thumbnail);
        img_cap2 = (ImageButton)findViewById(R.id.thumbnail2);
        img_cap3 = (ImageButton)findViewById(R.id.thumbnail3);
        img_cap4 = (ImageButton)findViewById(R.id.thumbnail4);

        img_cap.setOnClickListener(new BtnOnClick());
        img_cap2.setOnClickListener(new BtnOnClick());
        img_cap3.setOnClickListener(new BtnOnClick());
        img_cap4.setOnClickListener(new BtnOnClick());

        img_cap.setOnLongClickListener(new BtnOnLongClick());
        img_cap2.setOnLongClickListener(new BtnOnLongClick());
        img_cap3.setOnLongClickListener(new BtnOnLongClick());
        img_cap4.setOnLongClickListener(new BtnOnLongClick());

        if(file1.exists()){
            Bitmap thumbnail1 = decodeSampledBitmapFromFile(file1.getAbsolutePath(), 700, 700);
            img_cap.setImageBitmap(thumbnail1);
        }
        else{
            img_cap.setVisibility(View.GONE);
        }
        if(file2.exists()){
            Bitmap thumbnail2 = decodeSampledBitmapFromFile(file2.getAbsolutePath(), 700, 700);
            img_cap2.setImageBitmap(thumbnail2);
        }
        else{
            img_cap2.setVisibility(View.GONE);
        }
        if(file3.exists()){
            Bitmap thumbnail3 = decodeSampledBitmapFromFile(file3.getAbsolutePath(), 700, 700);
            img_cap3.setImageBitmap(thumbnail3);
        }
        else{
            img_cap3.setVisibility(View.GONE);
        }
        if(file4.exists()){
            Bitmap thumbnail4 = decodeSampledBitmapFromFile(file4.getAbsolutePath(), 700, 700);
            img_cap4.setImageBitmap(thumbnail4);
        }
        else{
            img_cap4.setVisibility(View.GONE);
        }

        /*if( !(file1.exists()) && !(file2.exists()) && !(file3.exists()) && !(file4.exists()) ){
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            File imageStorageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "Logistic_images" + File.separator + grp_id);
            if (!imageStorageFolder.exists()) {
                imageStorageFolder.mkdirs();
                Log.w("tag", "Folder created at: " + imageStorageFolder.toString());
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file1));
            link1 = file1.getAbsolutePath();
            startActivityForResult(intent, CAPTURE_IMAGE_THUMBNAIL_ACTIVITY_REQUEST_CODE);
        }*/



        cam_btn_1 =(ImageButton)findViewById(R.id.cam_btn);
        cam_btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String filepath = Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail1.jpg";
                Log.w("File Info", filepath.toString());

                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                //Check if your application folder exists in the external storage, if not create it:
                File imageStorageFolder = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id);
                if (!imageStorageFolder.exists())
                {
                    imageStorageFolder.mkdirs();
                    Log.w("tag" , "Folder created at: "+imageStorageFolder.toString());
                }

                int img_count=4;
                if(!file4.exists()){img_count=4;}
                if(!file3.exists()){img_count=3;}
                if(!file2.exists()){img_count=2;}
                if(!file1.exists()){img_count=1;}

                Log.w("img_count",Integer.toString(img_count));

                //Check if data in not null and extract the Bitmap:
                String filename = "thumbnail"+Integer.toString(img_count);
                String fileNameExtension = ".jpg";
                File sdCard = Environment.getExternalStorageDirectory();
                String imageStorageFolder2 = File.separator+"Logistic_images"+File.separator+grp_id+File.separator;

                File file = new File(
                        sdCard,
                        imageStorageFolder2 + filename + fileNameExtension
                );
                Log.w("tag", "the destination for image file is: " + file );

                if(img_count==1){link1 = file.getAbsolutePath();}
                if(img_count==2){link2 = file.getAbsolutePath();}
                if(img_count==3){link3 = file.getAbsolutePath();}
                if(img_count==4){link4 = file.getAbsolutePath();}

                //File file = new File(Environment.getExternalStorageDirectory()+File.separator + "image.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent, CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);

            }
        });

        update_btn = (Button)findViewById(R.id.btn_update);
        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                status_update(0);

            }
        });



        Calendar c = Calendar.getInstance();
        int minute = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int day = c.get(Calendar.DATE);
        String month = c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
        int year = c.get(Calendar.YEAR);

        String now = day + ", " + month + ", " + year + " " + hour + ":" + minute;
        TextView date = (TextView)findViewById(R.id.textView12);
        date.setText(now);

        get_details();
        get_product_list();



    }

    private void status_update2(int cancel) {
        //Update online database
        dba.open();
//        if (cancel == 0) {
//            if (damage_count > 0) {
//                dba.UpdateItem_s("db1_table", "col_0", do_no, "col_8", "4"); // Original Meaning: "3","有更改" ; Now to "4","部分完成"
//            } else {
//                dba.UpdateItem_s("db1_table", "col_0", do_no, "col_8", "3"); // Original Meaning: "2","已完成" ; Now to "3","已上載"
//            }
//        }
        if (cancel == 1) {
            dba.UpdateItem_s("db1_table", "col_0", do_no, "col_8", "8"); // Delivered
        }
        dba.UpdateItem_s("db1_table", "col_0", do_no, "col_7", f.time());
        dba.close();
        f.t(PageConfirm.this, "Case Updated");
    }

    private void status_update(int cancel){
        //Update online database
        dba.open();
        if(cancel==0){
            if(damage_count>0) {
                dba.UpdateItem_s("db1_table", "col_0", do_no, "col_8", "4"); // Original Meaning: "3","有更改" ; Now to "4","部分完成"
            }else{
                dba.UpdateItem_s("db1_table", "col_0", do_no, "col_8", "3"); // Original Meaning: "2","已完成" ; Now to "3","已上載"
            }
        }
        if(cancel==1){
            dba.UpdateItem_s("db1_table", "col_0", do_no, "col_8", "6"); // Original Meaning: "4","終止" ; Now to "6","取消"
        }
        dba.UpdateItem_s("db1_table", "col_0", do_no, "col_7", f.time());
        dba.close();
        f.t(PageConfirm.this, "Case Updated");


        try {

            SysSqlAdapter sql = new SysSqlAdapter(PageConfirm.this, 3, null, null);
            sql.delegate = this;
            if (cancel==0){
                if(damage_count>0) {
                    sql.execute("db1_table", "col_0", do_no, "col_8", "4");
                }else{
                    sql.execute("db1_table", "col_0", do_no, "col_8", "3");
                }
            }
            if(cancel==1){
                sql.execute("db1_table", "col_0", do_no, "col_8", "6");
            }
            sql.setListener(new CallBackListener() {
                @Override
                public void callback() {
                    if(return_pass.equals("1")){
                        Log.w("SQL", "Update success1");
                    }else{
                        Log.w("SQL","Update fail1");
                    }
                }
            });
        }catch (Exception e){
            Log.w("SQL","Reason : " + e);
        }

        try {

            SysSqlAdapter sql = new SysSqlAdapter(PageConfirm.this, 3, null, null);
            sql.delegate = this;
            sql.execute("db1_table", "col_0", do_no, "col_7", f.time());
            sql.setListener(new CallBackListener() {
                @Override
                public void callback() {
                    if(return_pass.equals("1")){
                        Log.w("SQL", "Update success2");
                        finish();
                    }else{
                        Log.w("SQL","Update fail2");
                    }
                }
            });
        }catch (Exception e){
            Log.w("SQL","Reason : " + e);
        }

        File file1 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail1.jpg");
        File file2 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail2.jpg");
        File file3 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail3.jpg");
        File file4 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail4.jpg");


        if(file4.exists())
        {
            DoImageUpload(do_no, file4.getPath(), PageMain.DLUsr);
            UploadFile2(file4.getPath(), do_no);
        }

        if(file3.exists())
        {
            DoImageUpload(do_no, file3.getPath(), PageMain.DLUsr);
            UploadFile2(file3.getPath(), do_no);
        }

        if(file2.exists())
        {
            DoImageUpload(do_no, file2.getPath(), PageMain.DLUsr);
            UploadFile2(file2.getPath(), do_no);
        }

        if(file1.exists())
        {
            //path = Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail1.jpg";
            //Log.w("Grab Image", grp_id + " " + link1 + " " + PageMain.DLUsr);
            DoImageUpload(do_no, file1.getPath(), PageMain.DLUsr);
            UploadFile2(file1.getPath(), do_no);
            //int Chknum = FileUpload.uploadFile("http://"+f.ip+"/clogistic/file_upload2.php", file1.getPath());
        }


    }

    public void UploadFile(String path){
//        try {
//            // Set your file path here
//            FileInputStream fstrm = new FileInputStream(path);
//            //FileInputStream fstrm = new FileInputStream(Environment.getExternalStorageDirectory().toString()+"/DCIM/file.mp4");
//
//            // Set your server page url (and the file title/description)
//            HttpFileUpload hfu = new HttpFileUpload("http://"+f.ip+"/clogistic/file_upload2.php", "my file title","my file description");
//
//            hfu.Send_Now(fstrm);
//
//        } catch (FileNotFoundException e) {
//            // Error: File not found
//        }

//        try
//        {
//            SimpleFTP ftp = new SimpleFTP();
//
//            // Connect to an FTP server on port 21.
//            ftp.connect("ftp.somewhere.net", 21, "username", "password");
//
//            // Set binary mode.
//            ftp.bin();
//
//            // Change to a new working directory on the FTP server.
//            ftp.cwd("web");
//
//            // Upload some files.
//            ftp.stor(new File("webcam.jpg"));
//            ftp.stor(new File("comicbot-latest.png"));
//
//            // You can also upload from an InputStream, e.g.
//            ftp.stor(new FileInputStream(new File("test.png")), "test.png");
//            ftp.stor(someSocket.getInputStream(), "blah.dat");
//
//            // Quit from the FTP server.
//            ftp.disconnect();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }

        //android upload file to server


            int serverResponseCode = 0;

            HttpURLConnection connection;
            DataOutputStream dataOutputStream;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";


            int bytesRead,bytesAvailable,bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File selectedFile = new File(path);


            String[] parts = path.split("/");
            final String fileName = parts[parts.length-1];

            if (!selectedFile.isFile()){
                //dialog.dismiss();

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        tvFileName.setText("Source File Doesn't Exist: " + selectedFilePath);
//                    }
//                });
                //return 0;
            }else{
                try{
                    String urllink = "http://"+f.ip+"/clogistic/file_upload2.php";

                    FileInputStream fileInputStream = new FileInputStream(selectedFile);
                    URL url = new URL(urllink);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);//Allow Inputs
                    connection.setDoOutput(true);//Allow Outputs
                    connection.setUseCaches(false);//Don't use a cached Copy
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    connection.setRequestProperty("uploaded_file",path);

                    //creating new dataoutputstream
                    dataOutputStream = new DataOutputStream(connection.getOutputStream());

                    //writing bytes to data outputstream
                    dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                            + path + "\"" + lineEnd);

                    dataOutputStream.writeBytes(lineEnd);

                    //returns no. of bytes present in fileInputStream
                    bytesAvailable = fileInputStream.available();
                    //selecting the buffer size as minimum of available bytes or 1 MB
                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
                    //setting the buffer as byte array of size of bufferSize
                    buffer = new byte[bufferSize];

                    //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);

                    //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                    while (bytesRead > 0){
                        //write the bytes read from inputstream
                        dataOutputStream.write(buffer,0,bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable,maxBufferSize);
                        bytesRead = fileInputStream.read(buffer,0,bufferSize);
                    }

                    dataOutputStream.writeBytes(lineEnd);
                    dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    serverResponseCode = connection.getResponseCode();
                    String serverResponseMessage = connection.getResponseMessage();

                    //Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);

                    //response code of 200 indicates the server status OK
                    if(serverResponseCode == 200){
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                tvFileName.setText("File Upload completed.\n\n You can see the uploaded file here: \n\n" + "http://coderefer.com/extras/uploads/"+ fileName);
//                            }
//                        });
                    }

                    //closing the input and output streams
                    fileInputStream.close();
                    dataOutputStream.flush();
                    dataOutputStream.close();



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(MainActivity.this,"File Not Found",Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Toast.makeText(MainActivity.this, "URL error!", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    //Toast.makeText(MainActivity.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
                }
//                dialog.dismiss();
//                return serverResponseCode;
            }


    }

    public void UploadFile2(String path, String header_no){

            //fileName="/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20151202-WA0014.jpg";
            //fileName="/sdcard/LSM_Screen.jpg";

        String url = "http://"+f.ip+"/clogistic/file_upload2.php";

            try {
                AsyncHttpClient client = new AsyncHttpClient();
                File myFile = new File(path);
                RequestParams params = new RequestParams();
                params.setForceMultipartEntityContentType(true);
                params.put("uploadedfile", myFile);
                params.put("header_no", header_no);

                client.post(getApplicationContext(), url, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                        String s = null;
                        try {
                            s = new String(responseBody, "UTF-8");
                            closeContextMenu();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        //theToast(s);
                    }

                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                        //theToast("Failed");
                        closeContextMenu();
                    }
                });


            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    public void DoImageUpload(String header_no, String path, String usr)
    {
        try {

            SysSqlAdapter sql = new SysSqlAdapter(PageConfirm.this, 5, null, null);
            sql.delegate = this;
            sql.execute(header_no, path, usr);
            sql.setListener(new CallBackListener() {
                @Override
                public void callback() {
                    if(return_pass.equals("1")){
                        Log.w("SQL", "Image Update success");
                        finish();
                    }else{
                        Log.w("SQL","Image Update fail");
                    }
                }
            });
        }catch (Exception e){
            Log.w("SQL","Reason : " + e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        Log.w("PageReg21",Integer.toString(resultCode));
        //Check that request code matches ours:
        if (requestCode == CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK)
        {
            Log.w("PageReg22",Integer.toString(resultCode));
            //Get our saved file into a bitmap object:

            if(link1!=null){
                File file = new File(link1);
                Bitmap thumbnail = decodeSampledBitmapFromFile(file.getAbsolutePath(), 700, 700);
                img_cap.setVisibility(View.VISIBLE);
                img_cap.setImageBitmap(getRoundedCornerBitmap(thumbnail,50));
            }

            if(link2!=null){
                File file = new File(link2);
                Bitmap thumbnail = decodeSampledBitmapFromFile(file.getAbsolutePath(), 700, 700);
                img_cap2.setVisibility(View.VISIBLE);
                img_cap2.setImageBitmap(getRoundedCornerBitmap(thumbnail,50));
            }

            if(link3!=null){
                File file = new File(link3);
                Bitmap thumbnail = decodeSampledBitmapFromFile(file.getAbsolutePath(), 700, 700);
                img_cap3.setVisibility(View.VISIBLE);
                img_cap3.setImageBitmap(getRoundedCornerBitmap(thumbnail,50));
            }

            if(link4!=null){
                File file = new File(link4);
                Bitmap thumbnail = decodeSampledBitmapFromFile(file.getAbsolutePath(), 700, 700);
                img_cap4.setVisibility(View.VISIBLE);
                img_cap4.setImageBitmap(getRoundedCornerBitmap(thumbnail,50));
            }

        }
        if(requestCode == CAPTURE_IMAGE_THUMBNAIL_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK)
        {
            Log.w("PageReg23",Integer.toString(resultCode));
            File file = new File(link1);
            Bitmap thumbnail = decodeSampledBitmapFromFile(file.getAbsolutePath(), 700, 1000);
            img_cap.setVisibility(View.VISIBLE);
            img_cap.setImageBitmap(getRoundedCornerBitmap(thumbnail,50));

        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
    { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_page_confirm, menu);
        setTitle(Html.fromHtml("<font color='#003f2e'>Arrived</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatemen
        if (id == R.id.cancel) {
            Log.w("cancel","1");
            cancel_case();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cancel_case() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PageConfirm.this);

        builder.setMessage("Confirm To Delete " + grp_id + "?")
                .setTitle("Delete case");

        AlertDialog.Builder confirm = builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                status_update(1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    private void confirm_dia_img(final String img_path, final int img_num) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PageConfirm.this);

        builder.setMessage("Confirm To Delete Image?")
                .setTitle("Delete image");

        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                File file = new File(img_path);

                file.delete();
                if (img_num == 1) {
                    img_cap.setVisibility(View.GONE);
                }
                if (img_num == 2) {
                    img_cap2.setVisibility(View.GONE);
                }
                if(img_num == 3){
                    img_cap3.setVisibility(View.GONE);
                }
                if(img_num == 4){
                    img_cap4.setVisibility(View.GONE);
                }


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private class BtnOnClick implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            grp_id = getIntent().getStringExtra("dono");

            File file1 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail1.jpg");
            File file2 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail2.jpg");
            File file3 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail3.jpg");
            File file4 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail4.jpg");

            switch(v.getId()) //get the id which is an int
            {
                case R.id.thumbnail : //if its button1 that is clicked
                    Log.w("Btn Click", "1");
                    Intent intent = new Intent(PageConfirm.this,MultiTouchActivity.class);
                    intent.putExtra("img_path", file1.getAbsolutePath());
                    startActivity(intent);
                    // use intents to pass information to secondActivity and display the image there
                    break;
                case R.id.thumbnail2 :
                    Log.w("Btn Click", "2");
                    Intent intent2 = new Intent(PageConfirm.this,MultiTouchActivity.class);
                    intent2.putExtra("img_path", file2.getAbsolutePath());
                    startActivity(intent2);
                    //use intents to pass information to secondActivity and display the image there
                    break;
                case R.id.thumbnail3 :
                    Log.w("Btn Click","3");
                    Intent intent3 = new Intent(PageConfirm.this,MultiTouchActivity.class);
                    intent3.putExtra("img_path", file3.getAbsolutePath());
                    startActivity(intent3);
                    //use intents to pass information to secondActivity and display the image there
                    break;
                case R.id.thumbnail4 :
                    Log.w("Btn Click","4");
                    Intent intent4 = new Intent(PageConfirm.this,MultiTouchActivity.class);
                    intent4.putExtra("img_path", file4.getAbsolutePath());
                    startActivity(intent4);
                    //use intents to pass information to secondActivity and display the image there
                    break;
            }

        }


    }

    private class BtnOnLongClick implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View v) {

            grp_id = getIntent().getStringExtra("dono");

            File file1 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail1.jpg");
            File file2 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail2.jpg");
            File file3 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail3.jpg");
            File file4 = new File(Environment.getExternalStorageDirectory()+File.separator+"Logistic_images"+File.separator+grp_id+File.separator+"thumbnail4.jpg");

            switch(v.getId()) //get the id which is an int
            {
                case R.id.thumbnail : //if its button1 that is clicked
                    Log.w("L Btn Click", "1");
                    confirm_dia_img(file1.getAbsolutePath(),1);
                    // use intents to pass information to secondActivity and display the image there
                    break;
                case R.id.thumbnail2 :
                    Log.w("L Btn Click","2");
                    confirm_dia_img(file2.getAbsolutePath(),2);
                    //use intents to pass information to secondActivity and display the image there
                    break;
                case R.id.thumbnail3 :
                    Log.w("L Btn Click","3");
                    confirm_dia_img(file3.getAbsolutePath(),3);
                    //use intents to pass information to secondActivity and display the image there
                    break;
                case R.id.thumbnail4 :
                    Log.w("L Btn Click","4");
                    confirm_dia_img(file4.getAbsolutePath(), 4);
                    //use intents to pass information to secondActivity and display the image there
                    break;
            }

            return true;
        }

    }

    public void processFinish(String output){
        return_pass = output;
    }

    private void get_details() {


        company = (TextView)findViewById(R.id.textView6);
        adrs = (TextView)findViewById(R.id.textView7);
        tel = (TextView)findViewById(R.id.textView8);
        time = (TextView)findViewById(R.id.textView10);


        dba.open();

        Cursor put = dba.GetSpecificItem("db1_table","col_0",grp_id);
        put.moveToFirst();

        Cursor put1 = dba.GetSpecificItem("db2_table","col_0",put.getString(2));
        put1.moveToFirst();

        Cursor put2 = dba.GetSpecificItem("db3_table","col_0",grp_id);
        put2.moveToFirst();

        String status = put.getString(9);
        if(status.equals("2")||status.equals("3")||status.equals("4")||status.equals("8")){
            //update_btn.setVisibility(View.GONE);
            close_btn = (Button)findViewById(R.id.btn_close);
            close_btn.setVisibility(View.GONE);
            update_btn.setText("Upload");

        }
        if (status.equals("1")){
            update_btn.setVisibility(View.VISIBLE);

            close_btn = (Button)findViewById(R.id.btn_close);
            close_btn.setVisibility(View.VISIBLE);
            close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    status_update2(1);
                    finish();

                }
            });
        }

        company.setText(put1.getString(2));
        adrs.setText(put1.getString(3) + ", " + put1.getString(4) + ", " + put1.getString(5));
        tel.setText(put1.getString(6));
        time.setText(put.getString(7).substring(0,2) + ":" + put.getString(7).substring(2,4));

        dba.close();

    }

    private void get_product_list() {
        dba.open();
        Cursor product = dba.GetSpecificItem("db3_table","col_0",grp_id);
        product.moveToFirst();

        Cursor put = dba.GetSpecificItem("db1_table","col_0",grp_id);
        put.moveToFirst();
        String status = put.getString(9);

        total_count = 0;
        stock_count = 0;
        damage_count = 0;
        less_count = 0;

        list2.clear();
        for (int row = 0; row < product.getCount(); row++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put("lv_1", product.getString(0));
            item.put("lv_2", product.getString(2));
            item.put("lv_3", product.getString(9));
            stock_count += Integer.parseInt(product.getString(9));
            item.put("lv_4", product.getString(10)); // Less
            less_count += Integer.parseInt(product.getString(10)); // Less = product.getString(10)  // Col_10 available to be "Damaged"
            damage_count += Integer.parseInt(product.getString(11)); // Less = product.getString(10)  // Col_10 available to be "Damaged"
            int balance = Integer.parseInt(product.getString(9)) - Integer.parseInt(product.getString(10));
            item.put("lv_5", balance + "");

            item.put("lv_6", product.getString(11)); // Damaged

            list2.add(item);


            product.moveToNext();
        }
        dba.close();

        total_count = stock_count - less_count - damage_count;
        Log.w("total",Integer.toString(total_count));
        Log.w("stock",Integer.toString(stock_count));
        Log.w("damage",Integer.toString(damage_count));

        ListView lv_pd = (ListView) findViewById(R.id.listView2);
        adapter1 = new SimpleAdapter(PageConfirm.this,
                list2,
                R.layout.listview_product,
                new String[]{"lv_1", "lv_2", "lv_3", "lv_4", "lv_5", "lv_6"},
                new int[]{R.id.notice_id, R.id.notice_col0, R.id.notice_col2, R.id.notice_col3, R.id.notice_col4, R.id.notice_col1});

                //new String[]{"lv_1", "lv_2", "lv_3", "lv_4"},
                //new int[]{R.id.notice_id, R.id.notice_col0, R.id.notice_col2, R.id.notice_col3});

        lv_pd.setAdapter(adapter1);
        lv_pd.setTextFilterEnabled(true);


        if(status.equals("1")){
            lv_pd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView tv_id = (TextView) view.findViewById(R.id.notice_id);
                    TextView tv_max = (TextView) view.findViewById(R.id.notice_col2);
                    TextView tv_product = (TextView) view.findViewById(R.id.notice_col0);

                    String real_id = tv_id.getText().toString();
                    String stock_code = tv_product.getText().toString();
                    int max_no = Integer.parseInt(tv_max.getText().toString());

                    damage_picker(real_id, max_no, stock_code);
                }
            });
        }

        lv_pd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });


        // Total Sum
        TextView tv_docqty, tv_less, tv_delivered, tv_damaged;
        tv_docqty = (TextView) findViewById(R.id.tv_docqty);
        tv_less = (TextView) findViewById(R.id.tv_less);
        tv_delivered = (TextView) findViewById(R.id.tv_delivered);
        tv_damaged = (TextView) findViewById(R.id.tv_damaged);

        String total = Integer.toString(total_count);
        String stock = Integer.toString(stock_count);
        String less = Integer.toString(less_count);
        String damage = Integer.toString(damage_count);

        tv_docqty.setText(stock);
        tv_less.setText(less);
        tv_delivered.setText(total);
        tv_damaged.setText(damage);

    }

    private void damage_picker(final String real_id, int max_no, final String stock_code){
        RelativeLayout linearLayout = new RelativeLayout(PageConfirm.this);
//        final NumberPicker aNumberPicker = new NumberPicker(PageConfirm.this);
//        aNumberPicker.setMaxValue(max_no);
//        aNumberPicker.setMinValue(0);

        final EditText input = new EditText(PageConfirm.this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setHint("No. of 'Less' Product");

//        final EditText input2 = new EditText(PageConfirm.this);
//        input2.setInputType(InputType.TYPE_CLASS_NUMBER);
//        input2.setHint("No. of 'Damaged' Product");


        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        //linearLayout.addView(aNumberPicker, numPicerParams);
        linearLayout.addView(input, numPicerParams);


//        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(50, 100);
//        RelativeLayout.LayoutParams numPicerParams2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        numPicerParams2.addRule(RelativeLayout.CENTER_HORIZONTAL);
//        linearLayout.setLayoutParams(params2);
//        linearLayout.addView(input2, numPicerParams2);


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PageConfirm.this);
        alertDialogBuilder.setTitle("Input No. of Product Affected");
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                //Log.e("","New Quantity Value : "+ aNumberPicker.getValue());
                                //input.getText().toString()

                                //local update
                                try {
                                    dba.open();
                                    dba.UpdateItem_s("db3_table", "_id", real_id, "col_9", input.getText().toString());
                                    //dba.UpdateItem_s("db3_table", "_id", real_id, "col_10", input2.getText().toString());
                                    //dba.UpdateItem_s("db3_table", "_id", real_id, "col_9", Integer.toString(aNumberPicker.getValue()));
                                    Log.w("PageConfirm", real_id);
                                    dba.close();
                                }catch(Exception e){
                                    Log.w("PageConfirm",e.toString());
                                }
                                //online update
                                SysSqlAdapter sql = new SysSqlAdapter(PageConfirm.this, 4, null, null);
                                sql.delegate = PageConfirm.this;
                                sql.execute("db3_table", "col_0", do_no, "col_1", stock_code, "col_9", input.getText().toString());
                                //sql.execute("db3_table", "col_0", do_no, "col_1", stock_code, "col_10", input2.getText().toString());
                                //sql.execute("db3_table", "col_0", do_no, "col_1", stock_code, "col_9", Integer.toString(aNumberPicker.getValue()));
                                sql.setListener(new CallBackListener() {
                                    @Override
                                    public void callback() {
                                        if (return_pass.equals("1")) {
                                            Log.w("SQL", "Update success");
                                            f.t(PageConfirm.this, "Uploaded to server");
                                        } else {
                                            Log.w("SQL", "Update fail");
                                        }
                                    }
                                });

                                get_product_list();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}